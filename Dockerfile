###########################################################################
# Dockerfile to build QueryBuilder demo container images
# Based on CentOS
###########################################################################

#version 1.0

# Set the base image to CentOS
 FROM centos:centos6

# File Author / Maintainer
 MAINTAINER 825338623@qq.com

#Install WGET
 RUN yum install -y wget

#Install tar
 RUN yum install -y tar

# Download JDK
RUN cd /opt;wget http://download.oracle.com/otn-pub/java/jdk/7u79-b15-demos/jdk-7u79-linux-x64-demos.tar.gz

#gunzip JDK
 RUN cd /opt;gunzip jdk-7u79-linux-x64-demos.tar.gz
 RUN cd /opt;tar xvf jdk-7u79-linux-x64-demos.tar
 RUN alternatives –install /usr/bin/java java /opt/jdk1.7.0_79/bin/java 2

# Download Apache Tomcat 7
 RUN cd /tmp;wget http://archive.apache.org/dist/tomcat/tomcat-7/v7.0.55/src/apache-tomcat-7.0.55-src.tar.gz

# untar and move to proper location
 RUN cd /tmp;gunzip apache-tomcat-7.0.55-src.tar.gz
 RUN cd /tmp;tar xvf apache-tomcat-7.0.55-src.tar
 RUN cd /tmp;mv apache-tomcat-7.0.55-src /opt/tomcat7
 RUN chmod -R 755 /opt/tomcat7

ENV JAVA_HOME /opt/jdk1.7.0_79

EXPOSE 8080