关于 QueryBuilder
-------------
>QueryBuilder是一个基于可视化复杂SQL拼接的一个插件，它使用了Bootstrap框架、Jquery.form、Jquery.validate等技术，支持浏览器缩放，支持主流浏览器心访问（IE8以上，Chrom,fireFox,Opra）。

QueryBuilder 的功能
-------------
>QueryBuilder是一个坚持通用花、大众化的设计理念，能够让非软件开发者轻松使用该插件进行复杂的SQL的可视化构建、、编辑、查询；本插件支持查询模板的保存功能。由于本插件使用了简洁、明了的开发框架，因此能够很好的移植、嵌入任何其他javaWeb项目中。

QueryBuilder功能规划
-------------
>一期项目完成基本的复杂SQL的可视化拼接查询，需要手动编辑配置文件。二期项目将采用配置数据源，插件自动生成配置文件，同时将支持多数据源、多种类型数据库的查询。

环境支持
-------------
>jdk:1.6 tomcat 1.6 myeclipse

QueryBuilder 涉及技术
-------------
>本源码使用了如下技术：Bootstrap、Sweetalert等前端框架或技术。

QueryBuilder 功能展示
-------------
#展示1：模板管理界面
>![模板管理](http://static.oschina.net/uploads/space/2016/0518/152546_eo7k_2303434.png)

声明
-------------
>本项目版权归属开发作者所有，仅限于研究学习之用，不得用于商业项目。未经许可，如已经发现，将付诸法律；如有意用于商业项目，请与作者联系，谢谢。

意见和建议
-------------
>在您使用 QueryBuilder 的过程中有任何意见和建议，请提交Issue或在项目下方的评论去进行评论，我们的开发者会对您的问题或意见进行优化或在这里跟您沟通。
在添加 Issue 时，请务必对问题或建议添加详细的描述。提出的 Issue 标题应当简明扼要，Issue 内容正文应当详细具体，最好提供问题的截图和修改建议。
欢迎通过[基于Web的自定义查询系统的开发](http://my.oschina.net/guopengfei/blog/479886)来了解本项目开发逻辑，同时欢迎访问我的博客[博客](http://my.oschina.net/guopengfei)。
欢迎加入我的QQ群进行技术交流:[QQ群链接](http://shang.qq.com/wpa/qunwpa?idkey=bda5b5a19dee571bad393db084f379e6eb2bdf068d2be05c0d511d857c03d654)
