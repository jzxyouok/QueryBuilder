<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'index.jsp' starting page</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!-- 新 Bootstrap 核心 CSS 文件 -->
<link rel="stylesheet"
	href="//cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- 可选的Bootstrap主题文件（一般不用引入） -->
<link rel="stylesheet"
	href="<%=path %>/css/bootstrap-theme.min.css">
<link rel="stylesheet"
	href="<%=path %>/css/main.css">

<!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
<script src="<%=path %>/js/jquery.min.js"></script>

<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
<script src="<%=path %>/js/bootstrap.min.js"></script>
</head>

<body>
	<div class="jumbotron">
		<div class="container">
			<h1>Hello, every body!</h1>
			<p>这是一个自定义高级查询模块的demo</p>
			<p>
				<a class="btn btn-primary btn-lg" href="<%=path %>/jumpDemo" role="button">进入demo</a>
			</p>
		</div>
	</div>



</body>
</html>
