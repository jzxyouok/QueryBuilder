<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/cmdb4.0.css" rel="stylesheet" type="text/css" />
<link href="../css/main.css" rel="stylesheet" type="text/css" />
<link href="../css/base.css" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='css/spectrum.css' />
<script src="../js/jquery.min.js"></script>
<script src='js/spectrum.js'></script>
<script src="js/classModelBuilder.js"></script>
<script src="js/inputFormat.js"></script>

<style type="text/css">
</style>
<script type="text/javascript">

	
	function getCSSPath(){
		var cssPath = 'localhost/';
		return cssPath;
	};
	function getImgPath(){
		var cssPath = 'kkkk/';
		return cssPath;
	};

</script>
</head>
<body>

	<input type="hidden" id="dataJson"  />

	<div id="browser" class="tbtree">
		<div class="tbopera">
			<div class="tbbtns">
				<a href="javascript:void(0);" onclick="refreshedModel('')">刷新</a><a
					id="exprot" href="javascript:void(0);" onclick="exportPro()">导出属性</a>
			</div>
			<div class="tbsearch">
				<!-- <input type="text" name="" id="" value="" /><a
					href="javascript:void(0);">搜索</a> -->
			</div>
		</div>
		<div class="tbtable" >
			<table id="leftTable" class="modelTable">
			</table>
		</div>
	</div>
	<div id="light" class="white_content">
		<div class="cmdb4_table_xj" >
			<table cellpadding="0" cellspacing="0" border="0" width="100%"
				align="center">
				<tr>
					<td width="200" class="tdno1">继承自：</td>
					<td width="210"><input class="xjinp" id="parVal"
						disabled="disabled"></td>
					<td width="190">&nbsp;</td>
				</tr>
				<tr id="classNameTr">
					<td class="tdno1"><span class="input_redstar ">*</span>中文名称：</td>
					<td><input class="xjinp" name="PLAN_NAME" type="text"
						id="className" hint="模型名称" allownull="false" />
					</td>
					<td>
						<div class="tdtips">错误提示</div>
					</td>
				</tr>
				<tr id="classTitleTr">
					<td class="tdno1"><span class="input_redstar ">*</span>英文名称：</td>
					<td><input class="xjinp" name="PLAN_NAME" type="text" maxlength="29"
						id="classTitle" hint="英文名称" onkeypress="return common.str_for(this,event)"  allownull="false" />
					</td>
					<td><div class="tdtips">错误提示</div></td>
				</tr>
				<tr>
					<td class="tdno1">&nbsp;</td>
					<td><input type="checkbox" id="isSession" value="1" /> <label
						for="isSession">是否缓存</label></td>
					<td>&nbsp;</td>
				</tr>
				<tr id="shunxuTr">
					<td class="tdno1"><span class="input_redstar "></span>顺序号：</td>
					<td><input class="xjinp" name="PLAN_NAME" type="text"
						id="shunxu" hint="顺序号" allownull="false"
						onkeypress="return common.num_verify(this,event)" /></td>
					<td><div class="tdtips">错误提示</div></td>
				</tr>
				<tr id="groupTr">
					<td class="tdno1">分组标识：</td>
					<td><select id="group">
							<option selected="selected" value="">--空--</option>
							<option value="服务记录">服务记录</option>
							<option value="资料配置">资料配置</option>
					</select></td>
					<td>&nbsp;</td>
				</tr>
				<tr id="link_tr" style="display:none">
					<td class="tdno1"><span class="input_redstar ">*</span>线条颜色：</td>
					<td><input class="xjinp" id="linkColor" /></td>
					<td>
						<div class="tdtips">错误提示</div></td>
				</tr>
				<tr id="linkWide_tr" style="display:none">
					<td class="tdno1"><span class="input_redstar ">*</span>线条粗细：</td>
					<td><input class="xjinp" id="linkWide"
						onkeypress="return common.num_int(this,event)" /></td>
					<td>
						<div class="tdtips">错误提示</div></td>
				</tr>
				<tr id="linkType_tr" style="display:none">
					<td class="tdno1"><span class="input_redstar ">*</span>线条样式：</td>
					<td><select id="linkType">
							<option value="-->">--></option>
							<option value="<--"><--</option>
							<option value="<-->"><--></option>
					</select></td>
					<td>
						<div class="tdtips">错误提示</div></td>
				</tr>
				<tr id="relationDescr_tr" style="display:none">
					<td class="tdno1">模型描述：</td>
					<td><input class="xjinp" id="relationDescr" /></td>
					<td></td>
				</tr>


			</table>
		</div>
		<div class="tbbtns">
			<a class="newDia" href="javascript:void(0);" id="submitForm"> 确定</a><a
				class="newDia" href="javascript:void(0);" id="closeDiv">取消</a>
		</div>
	</div>
	<div id="fade" class="black_overlay"></div>
	<form name="univeiwform" id="univeiwform" action="#" method="post">
	</form>
</body>
</html>
