<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>自定义高级查询</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!-- 新 Bootstrap 核心 CSS 文件 -->
<link rel="stylesheet"
	href="//cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- 可选的Bootstrap主题文件（一般不用引入） -->
<link rel="stylesheet" href="<%=path%>/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="<%=path%>/css/main.css">

<!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
<script src="<%=path%>/js/jquery.min.js"></script>

<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
<script src="<%=path%>/js/bootstrap.min.js"></script>

<script src="<%=path%>/js/main.js"></script>

</head>

<body class="queryDemoBody">
	<ul class="nav nav-tabs" id ="query_ul">
  <li role="presentation" id="query_template" class="active"><a href="javascript:void(0)">模板管理</a></li>
  <li role="presentation" id="builder_query"><a href="javascript:void(0)">查询结构管理</a></li>
  <li role="presentation" id="query_result"><a href="javascript:void(0)">查询结果管理</a></li>
</ul>
<div class="embed-responsive embed-responsive-4by3">
  <iframe class="embed-responsive-item" src="mainQuery/template.jsp"></iframe>
</div>
</body>
</html>
