/**
 * Created by pengfei guo on 2015/12/2.
 */
var common = {};
/**
 * 顺序字符输入 示例： 2.33.2334.23.34
 * @param data  当前输入框对象
 * @param e
 * @returns {boolean}
 */
common.num_verify = function(data,e) {
    //首先检查键入的是否是数字
    var keynum;
    var keychar;
    var numcheck;
    if (window.event) // IE
    {
        keynum = e.keyCode;
    } else if (e.which) // Netscape/Firefox/Opera
    {
        keynum = e.which;
    }

    var a = data.value;
    if(a == '' || a.lastIndexOf('.') == a.length-1){
        if(keynum == 46){
            return false;
        }
    }

    if ((keynum != 8 && keynum != 46 && keynum < 48) || keynum > 105
        || (keynum > 57 && keynum < 96)) {
        return false;
    }
    //Delete  BackSpace
    if (keynum == 8 || keynum == 46)
        return true;

    //判断删除和方向键
    keychar = String.fromCharCode(keynum);
    var numcheck = /\d/;
    if ((keynum != undefined && keychar != '.') && !numcheck.test(keychar)) {
        return false;
    }
};
/**
 * 允许输入整数
 * @param e
 * @returns {boolean}
 */
common.num_int = function (data, e) {
    //首先检查键入的是否是数字
    var keynum;
    var keychar;
    var numcheck;
    if (window.event) // IE
    {
        keynum = e.keyCode;
    } else if (e.which) // Netscape/Firefox/Opera
    {
        keynum = e.which;
    }
    if ((keynum != 8 && keynum < 48) || keynum > 105
        || (keynum > 57 && keynum < 96)) {
        return false;
    }
    //Delete  BackSpace
    if (keynum == 8)
        return true;

    //判断删除和方向键
    keychar = String.fromCharCode(keynum);
    var numcheck = /\d/;
    if ((keynum != undefined && keychar != '.') && !numcheck.test(keychar)) {
        return false;
    }
};


/**
 * 第一个输入的只允许输入字母，不可输入下划线
 * @param e
 * @returns {boolean}
 */
common.str_for = function (data, e) {
    //首先检查键入的是否是数字
    var keynum;
    var keychar;
    var numcheck;
    if (window.event) // IE
    {
        keynum = e.keyCode;
    } else if (e.which) // Netscape/Firefox/Opera
    {
        keynum = e.which;
    }

    var a = data.value.replace(/(^\s*)|(\s*$)/g,'');
    if(a == ''){
    	//第一个必须是字母
        if(!((keynum <=122 && keynum >=97)|| (keynum <=90 && keynum >=65))){
            return false;
        }
    }else if(!((keynum <=122 && keynum >=97)|| (keynum <=90 && keynum >=65) || (keynum <=57 && keynum >=48))){
    	//值允许输入数字和字母
        return false;
    }

    //Delete  BackSpace
    if (keynum == 8 || keynum == 46)
        return true;

    //判断删除和方向键
//    keychar = String.fromCharCode(keynum);
//    var numcheck = /\d/;
//    if ((keynum != undefined && keychar != '.') && !numcheck.test(keychar)) {
//        return false;
//    }
}