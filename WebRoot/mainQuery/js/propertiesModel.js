/**
 * 刷新数据
 */
var refushProperties = function (propertyTitle){
	$('#leftTable').empty();
	var classTitle = $('#classTitle').val();
	var modelType = $('#opType').val();
	var basePath = $("#basePath").val();
  　　var url =basePath+"servlet/ProperAndConsModelServlet";  
  	$.ajax({
		type:"POST",
		url:url,
		dataType:"text",
		data:{"opType":encodeURI("propertyModle"),
			"classTitle":encodeURI(classTitle),
			"modelType":encodeURI(modelType)},
		async: false,
		success:function(returnValue)
			{
			$("#dataJson").val(returnValue);
			buildBasic();
			if(propertyTitle == ''){
				propertyTitle='自身属性';
			}
			opMenu(propertyTitle);
			$('#leftTable tr').height(52);
			setTrWidth();
			},
		error:function(){
			alert("错误");	
		}
	})
};
/**
 * 遮罩层高都
 * @returns
 */
function getHeight(){
	var liulnaqiHei =  (parent.browerHei)/2; //浏览器可视化高度
    var scroHei = $(window.parent).scrollTop();  //滚动条高度
//    var parh = 52;
   return scroHei+liulnaqiHei+43;
}
/**
 * 根据类名获取类的节点
 * 
 * @param className
 */
var findDataByClsName = function (className){
    var currentDate ;
    var table = $('#leftTable');
    table.find('tr').each(function(i ,evTr){
        try{
            var currentVal = $(evTr).find(">td input[type='hidden']").val();
            if(currentVal.indexOf('自身属性') ==0 ){
            	var d =currentVal.length-className.length-1;
            	if((d>0&&currentVal.lastIndexOf('.'+className)==d)||currentVal == className){
            		currentDate =  $(evTr).find(">td input[type='hidden']").next()[0];
            		return false;
            	}
            }
        }catch (e){

        }
    });
    return currentDate;
};



/**
 * 获取所有的上级节点
 * 
 * @param data
 */
var findCanOp= function (className){
    var data = findDataByClsName(className);
    var tempData = [];
    var currentCls = $(data).prev().val();
    $(data).attr("class", "tbdiv");
    tempData.push(data);
    var tr = $(data).parent().parent().prev();
    while(tr.length == 1){
        try{
            var chiVal = tr.find(">td input[type='hidden']").val();
            if (currentCls.indexOf(chiVal+'.') == 0 && 1 == compareCount(chiVal, currentCls)) {
                currentCls = chiVal;
                var currentData = tr.find(">td input[type='hidden']").next();
                currentData.attr("class", "tbdiv");
                tempData.push(currentData[0]);
                // tempData.push(tr);

            }
            tr = tr.prev();
        }catch (e){
            tr = tr.prev();
        }
    }

    return tempData;
    // alert(tempData.length);
};

/**
 * 倒序打开菜单
 * 
 * @param tempData
 */
var opMenu = function (className){
    var tempData = findCanOp(className);
    for(var i=tempData.length-1;i>=0;i--){
        openOrCloseSecondMenu(tempData[i])
    }
};


$(function() {
	refushProperties("");
//	setIframeHeight();
});
/**
 * 表格的头部
 */
var buildBasic = function (){
	var temp = $("#dataJson").val();
	var data = eval('(' + temp + ')');
	var tr = '<tr><th scope="col"  class="pro_name" >属性标题</th><th scope="col" class="pro_title">英文名称</th><th scope="col" class="pro_tb">库表字段</th><th scope="col" class="pro_type">属性类型</th><th  scope="col" class="pro_cons">属性约束</th><th scope="col" class="pro_limit">属性限制</th><th scope="col" class="pro_note">说明信息</th> <th scope="col" class="tda">操作</th><th style="display: none" class="hid">隐藏属性</th></tr>';
	$("#leftTable").append(tr);
	var extPro = data.extPro;
	var selfPro = data.selfPro;
	builderMenu(extPro, '继承属性', 0);
	builderMenu(selfPro, '自身属性', 0);
// setTrWidth();
}

/**
 * 建立一级菜单，不可更改
 * 
 * @param data
 * @param proType
 * @param count
 */
var builderMenu = function(data, proType, count) {
	var tr = $("<tr>");
	tr.show();
	var td1 = $("<td class='pro_name'>");
// td1.attr('width', '140px');
	var hiddenIn;
	if (proType == '自身属性') {
		hiddenIn = $('<input type ="hidden" id="selfPro" value = "' + proType
				+ '"/>');
	} else {
		hiddenIn = $('<input type ="hidden" id="extPro" value = "' + proType
				+ '"/>');
	}
	td1.append(hiddenIn);
	var div = $("<div>");
	div.attr('style', 'padding-left:' + count + 'em');
	// if(proType == 'extPro'){
	div
			.append('<span class="spanl fuxuan"></span><span class="spanl tbimg"></span><span class="spanl tbname">'
					+ proType + '</span>');
	div.append('<span class="spanl"></span>');
	div.addClass('tbdiv');
	if (data.length > 0) {
		var span1 = $('<span class="spanr"><s class="tbdown"></s></span>');
		div.append(span1);
	} else {
		// 继承属性或滋生属性下没有子节点
		// 将此菜单设置为不可展开，没有下拉箭头
		var span1 = $('<span style="display:none" class="spanr"><s class="tbdown"></s></span>');
		div.append(span1);
	}

	div.bind("click", function() {
		openOrCloseSecondMenu(this);
	});
	td1.append(div);
	tr.append(td1);
	var td2 = ' <td classs="pro_title"></td><td classs="pro_tb"></td><td classs="pro_type"></td><td classs="pro_cons"></td><td classs="pro_limit"></td><td  classs="pro_note"></td><td class="tda"></td><td  class="pro_dis" style="display: none"></td>';
	tr.append(td1).append(td2);
	$('#leftTable').append(tr);
	if (data.length > 0) {
		count++;
		$.each(data, function(i, twoMenu) {
			builderSecondMenu(proType, twoMenu, count, false);
		});
	}

};
/**
 * 生成二级菜单
 * 
 * @param proType
 * @param data
 * @param count
 */
var builderSecondMenu = function(proType, data, count, isShow) {
	var twoMenuName = data.groupName;
	var tr = $("<tr>");
	if (isShow) {
		tr.show();
	} else {
		tr.hide();
	}
	var td1 = $('<td class="pro_name">');
// td1.attr('width', '140px');
	var hiddenIn;
	if (proType == '自身属性') {
		hiddenIn = $('<input type ="hidden" id="selfPro.' + twoMenuName
				+ '" value = "' + proType + '.' + twoMenuName + '"/>');
	} else {
		hiddenIn = $('<input type ="hidden" id="extPro.' + twoMenuName
				+ '" value = "' + proType + '.' + twoMenuName + '"/>');
	}
	td1.append(hiddenIn);
	var div = $("<div>");
	div.attr('style', 'padding-left:' + count + 'em');
	div
			.append('<span class="spanl fuxuan"></span><span class="spanl tbimg"></span><span class="spanl tbname">'
					+ twoMenuName + '</span>');
	// div.append('<span class="spanl"></span>');
	div.addClass('tbdiv');
	var span1;
	if (proType.indexOf('继承属性') == 0) {
		// 继承属性，不可修改
		span1 = $('<span class="spanl"></span><span   class="spanr"><s class="tbdown"></s></span>');
	} else {
		div.addClass('tbdiv');
		span1 = $('<span class="spanl"><a href="javascript:void(0);" class="tbedit"  onclick="updateGroupName(this);event.cancelBubble=true;"></a></span><span   class="spanr"><s class="tbdown"></s></span>');
	}

	div.append(span1);
	div.bind("click", function() {
		openOrCloseSecondMenu(this);
	});
	td1.append(div);
	tr.append(td1);
	var td2 = '<td classs="pro_title"></td><td classs="pro_tb"></td><td classs="pro_type"></td><td classs="pro_cons"></td><td classs="pro_limit"></td><td  classs="pro_note"></td><td class="tda"></td><td  class="pro_dis" style="display: none"></td>';
	tr.append(td1).append(td2);
	$('#leftTable').append(tr);
	var subMenu = data.subMenu;
	count++;
	$.each(subMenu, function(i, thirdMenu) {
		builderThirdMenuMenu(tr, proType + '.' + twoMenuName, thirdMenu, count,
				false);
	});

};
/**
 * 生成三级菜单
 * 
 * @param parName
 * @param thirdMenu
 * @param count
 */
var builderThirdMenuMenu = function(parTr, parName, thirdMenu, count, isShow) {
	var propertyDescr = thirdMenu.propertyDescr;
	var propertyTitle = thirdMenu.propertyTitle;
	var storageField = thirdMenu.storageField;
	var propertyType = thirdMenu.propertyType+'';
	// 属性限制 td
	var isCompare = thirdMenu.isCompare;
	var isSaveChange = thirdMenu.isSaveChange;
	var uiVisible = thirdMenu.uiVisible;
	var isMultLine = thirdMenu.isMultLine;
	var required = thirdMenu.required;
	var isAutoUpdate = thirdMenu.isAutoUpdate;
	var readonly = thirdMenu.readonly;

	var defaultValue = thirdMenu.defaultValue;
	var dimName = thirdMenu.dimName;
	var storageDatatype = thirdMenu.storageDatatype;
	var note = thirdMenu.note;
	var hasConstraint = thirdMenu.hasConstraint;
	var isBuiltinProperty  = thirdMenu.isBuiltinProperty;
	
	var tr = $("<tr>");
	if (isShow) {
		tr.show();
	} else {
		tr.hide();
	}
	var td1 = $("<td class='pro_name'>");
// td1.attr('width', '140px');
	var hiddenIn = $('<input type ="hidden" id="' + parName + '.'
			+ propertyTitle + '" value = "' + parName + '.' + propertyTitle
			+ '"/>');
	td1.append(hiddenIn);
	var div = $("<div>");
	div.attr('style', 'padding-left:' + count + 'em');
	div.append('<span class="spanl fuxuan"></span><span class="spanl tbimg"></span><span class="spanl tbname">'
					+ propertyDescr + '</span>');
	div.addClass('tbdiv');
	var span1 = $('<span class="spanl"></span><span  style="display:none" class="spanr"><s class="tbdown"></s></span>');
	div.append(span1);
	td1.append(div);
	tr.append(td1);
	var td2 = ' <td class="pro_title">'
			+ propertyTitle
			+ '</td><td class="pro_tb">'
			+ storageField
			+ '</td><td class="pro_type">'
			+ transformationFromNumToStr(propertyType)
			+ '</td>';
	if(hasConstraint){
		td2 +='<td class="pro_cons">有</td>';
	}else{
		td2 +='<td class="pro_cons">无</td>';
	}
	td2 += '<td class="pro_limit">'
			+ builderCheckBox(parName, propertyTitle, isCompare, '比较',
					'isCompare')
			+  builderCheckBox(parName, propertyTitle, uiVisible, '可见',
					'uiVisible')
			+ builderCheckBox(parName, propertyTitle, isMultLine, '唯一',
					'isMultLine')
			+ builderCheckBox(parName, propertyTitle, required, '必填',
					'required')
			+'<br/>'
			+ builderCheckBox(parName, propertyTitle, readonly, '只读',
					'readonly')
			+ builderCheckBox(parName, propertyTitle, isSaveChange, '记录变更',
					'isSaveChange')
			+ builderCheckBox(parName, propertyTitle, isAutoUpdate, '非关键属性',
					'isAutoUpdate') + '</td><td class="pro_note">' + note + '</td>';
	if (parName.indexOf('继承属性.') == 0) {
		td2 += '<td class="tda"></td>';
	} else {
		if(isBuiltinProperty){
			// 系统内建属性，不允许删除
			td2 += '<td class="tda"><a href="javascript:void(0);" onclick="newOrUpdatePro(this)" >修改</a><br/><a href="javascript:void(0);" onclick="proConstraint(this)">属性约束</a></td>';
		}else{
			td2 += '<td class="tda"><a href="javascript:void(0);" onclick="newOrUpdatePro(this)" >修改</a><a href="javascript:void(0);" onclick="deleteCurrentPro(this)">删除</a><br/><a href="javascript:void(0);" onclick="proConstraint(this)">属性约束</a></td>';
		}
	}
	var classTitle = $('#classTitle').val();
	td2 += '<td class="pro_dis" style="display: none" class="hid"><input type="hidden" id="' + classTitle
			+ '_defaultValue" value="' + defaultValue
			+ '"/><input type="hidden" id="' + classTitle + '_dimName" value="'
			+ dimName + '"/><input type="hidden" id="' + classTitle
			+ '_storageDatatype" value="' + storageDatatype + '"/></td>';
	tr.append(td1).append(td2);
// parTr.after(tr);
	$('#leftTable').append(tr);
	// $('#leftTable').append(tr);
};
/**
 * 生成一个checkBox
 * 
 * @param isTrue
 *            是否选定
 * @param name
 *            中文名称
 * @param title
 *            英文名称
 * @returns {*}
 */
var builderCheckBox = function(parName, propertyTitle, isTrue, name, title) {
	var checkBox;
	// if (parName.indexOf('继承属性.') == 0) {
	if (isTrue) {
		checkBox = '<input disabled="disabled" class="limit_check" type ="checkbox" id="'
				+ propertyTitle + '_' + title + '" name ="' + propertyTitle
				+ '_' + title + '" vlaue = "' + propertyTitle + '_' + title
				+ '" checked="checked"/><label class="limit_lab" for="' + propertyTitle + '_'
				+ title + '" >' + name + '</label>';
	} else {
		checkBox = '<input disabled="disabled" class="limit_check" type ="checkbox" id="'
				+ propertyTitle + '_' + title + '" name ="' + propertyTitle
				+ '_' + title + '" vlaue = "' + propertyTitle + '_' + title
				+ '"/><label class="limit_lab" for="' + propertyTitle + '_' + title + '" >'
				+ name + '</label>';
	}
	// } else {
	// if (isTrue) {
	// checkBox = '<input type ="checkbox" id="' + propertyTitle + '_' + title +
	// '" name ="' + propertyTitle + '_' + title + '" vlaue = "' + propertyTitle
	// + '_' + title + '" checked="checked"/><label for="' + propertyTitle + '_'
	// + title + '" >' + name + '</label>';
	// } else {
	// checkBox = '<input type ="checkbox" id="' + propertyTitle + '_' + title +
	// '" name ="' + propertyTitle + '_' + title + '" vlaue = "' + propertyTitle
	// + '_' + title + '"/><label for="' + propertyTitle + '_' + title + '" >' +
	// name + '</label>';
	// }
	// }
	return checkBox;
};

/**
 * 打开或关闭下级菜单
 * 
 * @param data
 * @returns
 */
var openOrCloseSecondMenu = function(data) {
	var classVal = $(data).attr("class");
	if (classVal == 'tbdiv tdcurrent') {
		// 关闭子菜单
		$(data).attr("class", "tbdiv");
		var parVal = $(data).parent().children('input[type="hidden"]').val();
		/* alert(inp); */
		var par = $(data).parent().parent();
		var nextTr = par.nextAll('tr');
		nextTr.each(function(m, chilNode) {
			$(chilNode).find(">td div[class='tbdiv tdcurrent']").attr("class",
					"tbdiv");
			var chiVal = $(chilNode).find(">td input[type='hidden']").val();
			if (chiVal.indexOf(parVal + '.') == 0) {
				$(chilNode).hide();
			}
		});
	} else {
		// 打开子菜单
		var parVal = $(data).parent().children('input[type="hidden"]').val();
		/* alert(inp); */
		var par = $(data).parent().parent();
		var nextTr = par.nextAll('tr');
		var flag = 'false';
		nextTr.each(function(m, chilNode) {
			var chiVal = $(chilNode).find(">td input[type='hidden']").val();
			if (chiVal.indexOf(parVal + '.') == 0
					&& 1 == compareCount(parVal, chiVal)) {
				// $(chilNode).display('bolck');
				// $(chilNode).css('display',"block");
				flag = 'true';
				$(chilNode).show();
			}
		});
		if (flag == 'true') {
			$(data).attr("class", "tbdiv tdcurrent");
		}
	}
 setTrWidth();
};

/**
 * 将数字类型转为字符串类型
 * 
 * @param num
 * @returns {*}
 */
var transformationFromNumToStr = function(num) {
	switch (num) {
	case '1':
		return '布尔型';
	case '2':
		return '整型';
	case '3':
		return '长整型';
	case '4':
		return '浮点型';
	case '5':
		return '日期';
	case '6':
		return '时间型';
	case '7':
		return '日期时间型';
	case '8':
		return '字符串型';
	case '9':
		return '列表型';
	case '10':
		return 'IP地址型';
	case '11':
		return '密码型';
	}
};
/**
 * 将字符串转为数字
 * 
 * @param str
 * @returns {number}
 */
var transformationFromStrToNum = function(str) {
	switch (str) {
	case '布尔型':
		return 1;
	case '整型':
		return 2;
	case '长整型':
		return 3;
	case '浮点型':
		return 4;
	case '日期':
		return 5;
	case '时间型':
		return 6;
	case '日期时间型':
		return 7;
	case '字符串型':
		return 8;
	case '列表型':
		return 9;
	case 'IP地址型':
		return 10;
	case '密码型':
		return 11;
	}
};

/**
 * 初始化默认值输入框
 */
var intitDefaultVal = function (val,defaultVal){
	var returnVal = {};
	var name = '';
	if(val == '1' || val == 1 ){
		// 布尔型
		name = '请输入  是  或   否';
		if(defaultVal == ''){
			defaultVal = name;
		}else if(defaultVal == '1' || defaultVal == 1){
			defaultVal = '是';
		}else if(defaultVal == '0' || defaultVal == 0){
			defaultVal = '否';
		}
	}else if(val == '2' || val == 2 ){
		// 整型
		name = '请输入整数';
		if(defaultVal==''){
			defaultVal = name;
		}
	}else if(val == '3' || val == 3 ){
		// 长整型
		name = '请输入整数';
		if(defaultVal==''){
			defaultVal = name;
		}
	}else if(val == '4' || val == 4 ){
		// 浮点型
		name = '请输入数字';
		if(defaultVal==''){
			defaultVal = name;
		}
	}else if(val == '5' || val == 5 ){
		// 日期行
		name = '示例：2015-12-08';
		if(defaultVal==''){
			defaultVal = name;
		}
	}else if(val == '6' || val == 6 ){
		// 时间型
		name ='示例: 14:49:15';
		if(defaultVal==''){
			defaultVal = name;
		}
	}else if(val == '7' || val == 7 ){
		// 时间日期型
		name ='示例： 2015-12-14 14:49:15';
		if(defaultVal==''){
			defaultVal = name;
		}
	}else if(val == '8' || val == 8 ){
		// 字符串型
		name ='多个值请以 | 分隔';
		if(defaultVal==''){
			defaultVal = name;
		}
	}else if(val == '9' || val == 9 ){
		// 列表型
		name ='请输入资源对象';
		$('#dimName').attr("disabled", false);
		var modeData = eval('(' + $('#modeData').val() + ")");
		initRel_pro(modeData, 'dimName');
		if(defaultVal==''){
			defaultVal = name;
		}
		$('#dimName').change(function(){
			$('#defaultTd').empty();
			$('#defaultTd').append('<input class="xjinp" name="请输入资源对象" type="text" id="defaultVal" hint="默认值" allownull="false" style="color:#999"/>');
			$('#defaultVal').val("请输入资源对象");
			defaultValEvent(val);
		});
	}else if(val == '10' || val == 10 ){
		// IP地址型
		name ='示例：192.168.101.1';
		if(defaultVal==''){
			defaultVal = name;
		}
	}else if(val == '11' || val == 11 ){
		// 密码型
		name ='请输入密码';
		if(defaultVal==''){
			defaultVal = name;
		}
	}
	returnVal.name = name;
	returnVal.defaultVal = defaultVal;
	return returnVal;
}
/**
 * 新增属性或修改属性
 * 
 * @param data
 */
function newOrUpdatePro(data) {
	var opType ;
	var proTypeTd;
	var defaultValue;
	var dimName ;
// $('#classTitleTr').removeClass();
// $('#classNameTr').removeClass();
// $('#dimNameTr').removeClass();
	removeAllClass();
	if (data == null || data == undefined) {
		// 新增属性
		proTypeTd ='布尔型';
		opType = "add";
		defaultValue ='';
		dimName = '';
		// $("#submitForm").unbind("click");
		$('#proTitle').attr("disabled", false).val("");
		$('#dimName').attr("disabled", true).val("");
		$('#proName').val("");
		$('#proGroup').val("");
		$('#defaultVal').val("");
		// $('#storageDatatype').attr("disabled", false).val('');
		$('#storageDatatype').val('');
		$('#storageField').attr("disabled", true).val("");
		$('#note').val('');
		var tempLimit = $('#proLimit').children('input[type="checkbox"]');
		tempLimit.each(function(i, check) {
			$(check).attr('checked', false);
		});
		$('#proType').attr("disabled",false);
		$('#proType').change();
	} else {
		// 修改属性
		// 属性标题不可修改
		opType = "update";
		var tr = $(data).parent().parent(); //
		var title = tr.children().eq('1').html();
		
		dimName = $(data).parent().next().children().eq('1').val();
		
		
		var storageDatatype = $(data).parent().next().children().eq('2').val();
		var tempType = storageDatatype.substring(0, storageDatatype
				.indexOf('('));
		storageDatatype = transDatatypeFrNaToNum(storageDatatype);
		$('#storageDatatype').val(storageDatatype).attr('name', tempType);

		var storageField = tr.children().eq('2').html();
		$('#storageField').attr('disabled', true).val(storageField);

		$('#proTitle').val(title); // 属性标题
		$('#proTitle').attr('disabled', true);
		
		var proType = tr.children().eq('3').html();
		proType = transformationFromStrToNum(proType);
		$('#proType').attr("disabled",true);
		var name = tr.children('td:first-child').find('>div span').eq('2')
				.html();
		$('#proName').val(name); // 属性中文名称
		var parVal = tr.children('td:first-child').children(
				'input[type="hidden"]').val();
		var tempClassTitle = parVal.substr(0, parVal.lastIndexOf('.'));
		// 组名设置
		var flag = false;
		var parTr;
		var parTitle;
		while (flag == false) {
			tr = tr.prev();
			parTitle = tr.find('> td input[type="hidden"').val();
			if (parTitle == tempClassTitle) {
				// 找到父级tr
				parTr = tr;
				flag = true;
			} else if (tr == 'undefined') {
				// 已经遍历到顶部没有找到父级,不存在此情况
				break;
			}
		}
		if (flag) {
			var tempTitle = parTitle.substr(parTitle.lastIndexOf('.') + 1);
			$('#proGroup').val(tempTitle);
		}
//		// 默认属性类型
		proTypeTd = $(data).parent().prev().prev().prev().prev().html();
//		setSelect(proTypeTd);
		defaultValue = $(data).parent().next().children().eq('0').val();
		
		// 获取属性限制的值
		var limitTd = $(data).parent().prev().prev().children(
				'input[type="checkbox"]');
		var tempLimit = $('#proLimit').children('input[type="checkbox"]');
		limitTd.each(function(i, check) {
			var a = $(check).is(':checked');
			$(tempLimit[i]).attr('checked', a);
		});
		
		var note = $(data).parent().prev().html();
		$('#note').val(note);

	}
// $('#proType').change();
	// 默认属性类型
	
	setSelect(proTypeTd);
	var proTypeSel = $('#proType')[0];
	checkChangeType(proTypeSel,defaultValue,dimName,opType);
	
	document.getElementById('light').style.display = 'block';
	document.getElementById('fade').style.display = 'block';
	var divLi = document.getElementById('light');
// $(divLi).height(340);
// $(divLi).css("top",'235px');
	
	
	var divLi = document.getElementById('light');
	$(divLi).height(380);
	var divLiTop = getHeight();
	$(divLi).css("top",(divLiTop-190)+'px');
//	setDivCenter(document.getElementById('light'));
	
	
	// 绑定确定按钮事件
	$("#submitForm").bind(
			"click",
			function() {
// $('#classTitleTr').removeClass();
// $('#classNameTr').removeClass();
// $('#dimNameTr').removeClass();
				removeAllClass();
				// 点击确定按钮
				var groupName = $('#proGroup').val().replace(/(^\s*)|(\s*$)/g, "");
				if(groupName ==''){
					groupName = '基本属性';
				}
				var proTitle = $('#proTitle').val();
				var proName = $('#proName').val();
				var tempCheckBox = $('#proLimit').children(
						'input[type="checkbox"]');
				var proType = $("#proType ").find("option:selected").val();
				var defaultVal = $('#defaultVal').val();
				var dimName = $('#dimName').val();
				var storageField = $('#storageField').val();
				var storageDatatype = $('#storageDatatype').attr('name')+"("+$('#storageDatatype').val()+")";
				// 新增的操作
				var proTitle = $('#proTitle').val();
				var flag;
				
// flag = checkIsNull();
				var returnVal = checkIsNull(opType);
				if(returnVal.checkType){
					// 存在空的情况，不继续执行
					return;
				}
				defaultVal = returnVal.value;
				flag = checkProTitleHasExist(opType, groupName, proName,
						proTitle, proType, tempCheckBox, defaultVal,
						dimName, storageField, storageDatatype);
				// 检查新增属性标题是否和原先存在的属性标题重复
				if(flag){
					resetDiv();
					refushProperties(groupName);
				}else{
					$('#classTitleTr').addClass("errortd");
					$('#classTitleTr').find('td:last-child').find('div')
							.html("与现有名称重复！");
				}
			});
	// 绑定取消按钮事件
	$("#closeDiv").bind("click", function() {
		// 点击取消按钮
		resetDiv();
	});
};

/**
 * $('#dimName').change(function(){ var temp = $(this).val(); var modeData =
 * eval('('+$('#modeData').val()+')'); var flag = checkBootModel(temp,modeData);
 * if(flag){ var classTitle =
 * temp.substring(temp.indexOf('(')+1,temp.indexOf(')'));
 * getAllDefaltVal(classTitle); } });
 */
function setDivCenter(divName){   
    var top = ($(window).height() - $(divName).height())/2;   
// var left = ($(window).width() - $(divName).width())/2;
    var scrollTop = document.body.scrollHeight + "px"; 
    var scrollLeft = $(document).scrollLeft();   
    $(divName).css( { position : 'absolute', 'top' : top + scrollTop } ).show();  
}
/**
 * 检查必填项是否为空
 * @param opType 操作
 * @returns
 */
var checkIsNull = function (opType){
	var proTitle = $('#proTitle').val().replace(/(^\s*)|(\s*$)/g, "");
	var proName = $('#proName').val().replace(/(^\s*)|(\s*$)/g, "");
	var dimName = $('#dimName').val().replace(/(^\s*)|(\s*$)/g, "");
	var defaultVal = $('#defaultVal').val().replace(/(^\s*)|(\s*$)/g, "");
	var proGroup = $('#proGroup').val().replace(/(^\s*)|(\s*$)/g, "");
	var note = $('#note').val().replace(/(^\s*)|(\s*$)/g, "");
	var type = $('#proType').val();
	var storageDatatype = $('#storageDatatype').val().replace(/(^\s*)|(\s*$)/g, "");
	var modeData =[];
	try{
		var modelDataStr =  $('#modeData').val();
		modeData = eval("("+modelDataStr+")");
	}catch (e){
		
	}
	var flag = false;
	
	if(proTitle == ''){
		$('#classTitleTr').addClass("errortd");
		$('#classTitleTr').find('td:last-child').find('div')
				.html("不能为空！");
		flag = true;
	}else  if(!/^[a-zA-Z][0-9a-zA-Z]*$/.test(proTitle.replace(/(^\s*)|(\s*$)/g,''))) {
		 // 只能为字母和数字，字符串不得以数字开头，不得包含除此之外的其他字符
		$('#classTitleTr').addClass("errortd");
		$('#classTitleTr').find('td:last-child').find('div')
				.html("只能为字母和数字，且不能以数字开头！");
		flag = true;
		 
	 }
	if(proName.replace(/(^\s*)|(\s*$)/g,'') == '' || proName.replace(/(^\s*)|(\s*$)/g,'').length>9){
		$('#classNameTr').addClass("errortd");
		$('#classNameTr').find('td:last-child').find('div')
				.html("长度在1-9之间！");
		flag = true;
	}else if(isExistProName(opType,proTitle,proName.replace(/(^\s*)|(\s*$)/g,''))){
		$('#classNameTr').addClass("errortd");
		$('#classNameTr').find('td:last-child').find('div')
				.html("此名称已经存在！");
		flag = true;
	}
	
	if(proGroup.replace(/(^\s*)|(\s*$)/g,'').length>15){
		$('#proGroupTr').addClass("errortd");
		$('#proGroupTr').find('td:last-child').find('div')
				.html("长度在1-15之间！");
		flag = true;
	}
	if(type == 9 || type =='9'){
		if(dimName == ''){
			$('#dimNameTr').addClass("errortd");
			$('#dimNameTr').find('td:last-child').find('div')
					.html("不能为空！");
			flag = true;
		} 
		
		if(!flag && !checkBootModel(dimName,modeData)){
			$('#dimNameTr').addClass("errortd");
			$('#dimNameTr').find('td:last-child').find('div')
					.html("对应对象不存在！");
			flag = true;
		}
	}
	
	if (! /^\d+$/.test(storageDatatype.replace(/(^\s*)|(\s*$)/g,'')) || storageDatatype.replace(/(^\s*)|(\s*$)/g,'') == '0') {
		// 不全是数字
		$('#storageDatatypeTr').addClass("errortd");
		$('#storageDatatypeTr').find('td:last-child').find('div')
				.html("必须输入大于0的数字！");
		flag = true;
	}
	
	if(note.length>100){
		$('#note_tr').addClass("errortd");
		$('#note_tr').find('td:last-child').find('div')
				.html("长度超过了100字符！");
		flag = true;
	}
	var returnVal = checkDefaultVal(defaultVal);
	if(flag || returnVal.checkType){
		returnVal.checkType = true;
	}else{
		// 验证长度是否符合
		if((returnVal.value+"").length > parseInt(storageDatatype)){
			$('#defaultTr').addClass("errortd");
			$('#defaultTr').find('td:last-child').find('div')
					.html("默认值长度超出范围！");
			returnVal.checkType = true;
		}
	}
	return returnVal;
};
/**
 * 判断是否存在相同的中文名称
 */
var isExistProName = function (opType,proTitle,proName){
	var isExist = false;
	if(opType=='add'){
		$('#leftTable').find('tr').each(function(i,trNode){
			var tempProName = "";
			try{
				tempProName= $(trNode).find(">td div span").eq(2).html().replace(/(^\s*)|(\s*$)/g,'');
				if (proName == tempProName) {
					isExist = true;
					return false;
				}
			}catch(e){
				console.log("此异常属于正常现象，不处理！");
			}
		});
	}else if(opType =='update'){
		$('#leftTable').find('tr').each(function(i,trNode){
			var tempProName = "";
			var tempProTitle ="";
			try{
				tempProName= $(trNode).find(">td div span").eq(2).html().replace(/(^\s*)|(\s*$)/g,'');
				tempProTitle = $(trNode).find(">td").eq(1).html();
				if (proName == tempProName && tempProTitle !=proTitle) {
						isExist = true;
						return false;
				}
			}catch(e){
				console.log("此异常属于正常现象，不处理！");
			}
		});
	}
	
	return isExist;
}
/**
 * 验证默认值
 */
var checkDefaultVal = function (temp1){
	var returnVal = {};
	var modelData ;
	if(temp1 ==''){
		returnVal.checkType= false;
		return returnVal;
	}
	var val = $('#proType').val();
	var flag = false;
	if(val =='1' || val == 1){
		// 布尔型
		if(temp1 == '请输入  是  或   否'){
			returnVal.value='';
		}else if( temp1 == '是'){
			returnVal.value='1';
		}else if(temp1 =='否'){
			returnVal.value='0';
		}else{
			flag = true;
		}
	}else if(val =='2' || val == 2){
		// 整型
		if(temp1 == '请输入整数'){
			returnVal.value='';
		}else if(!/^[1-9]+[0-9]*$/.test(temp1)){
			flag = true;
		}else {
			returnVal.value=parseInt(temp1);
		}
	}else if(val =='3' || val == 3){
		// 长整形
		if(temp1 == '请输入整数'){
			returnVal.value='';
		}else if(!/^[1-9]+[0-9]*$/.test(temp1)){
			flag = true;
		}else {
			returnVal.value=parseInt(temp1);
		}
	}else if(val =='4' || val == 4){
		// 浮点型
		if(temp1 == '请输入数字'){
			returnVal.value='';
		}else if(isNaN(temp1)){
			flag = true;
		}else{
			returnVal.value=parseFloat(temp1);
		}
	}else if(val =='5' || val == 5){
		// 日期行
		var datePattern = /^(?:19|20)[0-9][0-9]-(?:(?:0[1-9])|(?:1[0-2]))-(?:(?:[0-2][1-9])|(?:[1-3][0-1]))$/;
		if(temp1 == '示例：2015-12-08'){
			returnVal.value='';
		}else if(!datePattern.test(temp1)){
			flag = true;
		}else{
			returnVal.value=temp1;
		}
	}else if(val =='6' || val == 6){
		// 时间型
		var datePattern = /^(?:(?:[0-2][0-3])|(?:[0-1][0-9])):[0-5][0-9]:[0-5][0-9]$/;
		if(temp1 == '示例: 14:49:15'){
			returnVal.value='';
		}else if(!datePattern.test(temp1)){
			flag = true;
		}else {
			returnVal.value=temp1;
		}
	}else if(val =='7' || val == 7){
		// 日期时间型
		var reDateTime = /^(?:19|20)[0-9][0-9]-(?:(?:0[1-9])|(?:1[0-2]))-(?:(?:[0-2][1-9])|(?:[1-3][0-1])) (?:(?:[0-2][0-3])|(?:[0-1][0-9])):[0-5][0-9]:[0-5][0-9]$/;
		if(temp1 == '示例： 2015-12-14 14:49:15'){
			returnVal.value='';
		}else if(!reDateTime.test(temp1)){
			flag = true;
		}else {
			returnVal.value=temp1;
		}
	}else if(val =='8' || val == 8){
		// 不验证
		if(temp1 =='多个值请以 | 分隔'){
			returnVal.value='';
		}else{
			returnVal.value=temp1;
		}
	}else if(val =='9' || val == 9){
		modelData = eval('('+$('#defaultModel').val()+")");
		if(temp1 =='请输入资源对象'){
			returnVal.value='';
		}else if(!checkBootModel(temp1,modelData)){
			flag = true;
		}else{
			var temp2 = getdefaultResId(temp1,modelData);
			returnVal.value=temp2;
		}
	}else if(val =='10' || val == 10){
		// ip地址
		var re = /([0-9]{1,3}\.{1}){3}[0-9]{1,3}/;
		if(temp1 !='示例：192.168.101.1' &&  !re.test(temp1)){
			flag = true;
		}else{
			returnVal.value=temp1;
		}
	}else if(val =='11' || val == 11){
		// 不验证，密码行
		if(temp1 =='请输入密码'){
			returnVal.value='';
		}else{
			returnVal.value=temp1;
		}
	}
	if(flag){
		$('#defaultTr').addClass("errortd");
		if(modelData !=undefined && modelData !='undefined' && modelData.length ==0){
			$('#defaultTr').find('td:last-child').find('div')
			.html("所选对应对象没有资源！");
		}else{
			$('#defaultTr').find('td:last-child').find('div')
			.html("请按要求填写！");
		}
		returnVal.checkType=true;
	}else{
		returnVal.checkType=false;
	}
	return returnVal;
}

/**
 * 判断数组中是否包含输入的值
 * 
 * @param modelName
 *            输入的值
 * @param modeData
 *            数组对象
 * @returns {boolean}
 */
var checkBootModel = function (modelName,modeData){
    var  flag = false;
    // modeData 格式如下：
    // ["状态(phase)-$-zhuangtai","父对象(parentResID)-$-fuduixiang","创建人员(createdAccount)-$-chuangjianrenyuan","资源类型(resCategory)-$-ziyuanleixing","CI配置管理员(resCiAdmin)-$-CIpeizhiguanliyuan","关键级别(priority)-$-guanjianjibie","最后修改人员(lastUpdatedAccount)-$-zuihouxiugairenyuan"];
   ;
    $.each(modeData, function(i, tempData) {
        var temp1 = tempData.split('-\$-');
        if(modelName == temp1[0]){
            flag = true;
            return false;
        }
    });
    return flag;
};

/**
 * 获取默认值的resID
 */
var getdefaultResId = function (modelName,modeData){
    var  resId = "";
    // modeData 格式如下：
    // ["状态(phase)-$-zhuangtai","父对象(parentResID)-$-fuduixiang","创建人员(createdAccount)-$-chuangjianrenyuan","资源类型(resCategory)-$-ziyuanleixing","CI配置管理员(resCiAdmin)-$-CIpeizhiguanliyuan","关键级别(priority)-$-guanjianjibie","最后修改人员(lastUpdatedAccount)-$-zuihouxiugairenyuan"];
    $.each(modeData, function(i, tempData) {
        var temp1 = tempData.split('-\$-');
        if(modelName == temp1[0]){
        	resId = temp1[1];
            return false;
        }
    });
    return resId;
};
/**
 * 移除所有的错误提示样式
 * 
 * @returns
 */
var removeAllClass = function (){
	$('#classTitleTr').removeClass();
	$('#classNameTr').removeClass();
	$('#dimNameTr').removeClass();
	$('#storageDatatypeTr').removeClass();
	$('#proGroupTr').removeClass();
	$('#defaultTr').removeClass();
	$('#note_tr').removeClass();
	
};

/**
 * 检测输入的字符串是否符合输入规则 字符(字母或数字)
 * 
 * @param str
 * @returns true 符合 false 不符合
 */
var  isFormatInput = function (str){
// str = str.replace(/(^\s*)|(\s*$)/g, "");
	var firIn = str.indexOf('(');
	var lasIn = str.indexOf(')');
	var count = str.length-1;
	if(firIn>0 && lasIn== count){
		return true;
	}else{
		return false;
	}
	
}
/**
 * 将完整测storageDatatype切割成纯数字
 */
var transDatatypeFrNaToNum = function(str) {
	var a11 = str.substring(str.indexOf('(') + 1, str.indexOf(')'));
	return a11;
}

/**
 * 重置属性维护界面
 */
var resetDiv = function() {
	$("#submitForm").unbind("click");
// $('#classTitleTr').removeClass();
// $('#classNameTr').removeClass();
	removeAllClass();
	$('#proTitle').val("");
	$('#proName').val("");
	$('#proGroup').val("");
	$('#defaultVal').val("");
	document.getElementById('light').style.display = 'none';
	document.getElementById('fade').style.display = 'none';
};
/**
 * 设置下拉默认选中
 * 
 * @param str
 *            将要被选中的下拉中的值
 */
function setSelect(str) {
	var temp = transformationFromStrToNum(str);
	$('#proType option').each(function(i, option) {
		var val = $(option).val();
		if (val == temp)
			$(option).attr('selected', true);
	});
};
/**
 * 检查属性标题是否已经存在
 * 
 * @param proTitle
 *            属性标题
 */
function checkProTitleHasExist(optionType, groupName, proName, proTitle,
		proType, tempCheckBox, defaultVal, dimName, storageField,
		storageDatatype) {
	var flag = false;
	var isCompare = $(tempCheckBox[0]).is(':checked');
	var uiVisible = $(tempCheckBox[1]).is(':checked');
	var isMultLine = $(tempCheckBox[2]).is(':checked');
	var required = $(tempCheckBox[3]).is(':checked');
	var readonly = $(tempCheckBox[4]).is(':checked');
	var isSaveChange = $(tempCheckBox[5]).is(':checked');
	var isAutoUpdate = $(tempCheckBox[6]).is(':checked');
	var classTitle = $('#classTitle').val();
	var note = $('#note').val();
	var basePath = $("#basePath").val();
	var url = basePath + "servlet/UpdateClassServlet";
	var userAccount = $('#userAccount').val();
	$.ajax({
		type : "POST",
		url : url,
		dataType : "json",
		data : {
			"optionType" : encodeURI(optionType),
			"opClass" : encodeURI("ResDefProperty"),
			"classTitle" : encodeURI(classTitle),
			"proTitle" : encodeURI(proTitle),
			"proName" : encodeURI(proName),
			"groupName" : encodeURI(groupName),
			"proType" : encodeURI(proType),
			"defaultVal" : encodeURI(defaultVal),
			"dimName" : encodeURI(dimName),
			"storageField" : encodeURI(storageField),
			"storageDatatype" : encodeURI(storageDatatype),
			"isCompare" : encodeURI(isCompare),
			"isSaveChange" : encodeURI(isSaveChange),
			"uiVisible" : encodeURI(uiVisible),
			"isMultLine" : encodeURI(isMultLine),
			"required" : encodeURI(required),
			"readonly" : encodeURI(readonly),
			"isAutoUpdate" : encodeURI(isAutoUpdate),
			"note" : encodeURI(note),
			"userAccount":encodeURI(userAccount)
		},
		async : false,
		success : function(returnValue) {
			flag = returnValue.newOrupdate;
		},
		error : function() {
			alert("错误");
		}
	});
	return flag;
};
/**
 * 删除一个属性
 * 
 * @param data
 *            点击删除时所触发的div
 */
var deleteCurrentPro = function(data) {
	var flag = confirm("确定删除吗？");
	if(!flag){
		return ;
	}
	// 1.删除当前行，
	// 1.1 如果此分组内只有一行属性数据，则删除该分组
	var tr = $(data).parent().parent();
	var currFullName = tr.children('td:first-child').children(
			'input[type="hidden"]').val();
	var deleType = deleteRow(currFullName);
	if(!deleType){
		alert("删除失败！");
		return false;
	}
	var flag = false;
	var parTr;
	var fullName;
	var currntTr = tr;
	while (flag == false) {
		currntTr = currntTr.prev();
		fullName = currntTr.find('> td input[type="hidden"').val();
		if (currFullName.indexOf(fullName + ".") == 0
				&& 1 == compareCount(fullName, currFullName)) {
			// 找到父级tr
			parTr = currntTr;
			flag = true;
		} else if (currntTr == 'undefined') {
			// 已经遍历到顶部没有找到父级，说明要删除的是顶级元素，不允许删除定义元素
			break;
		}
	}
	// 找到父级tr
	if (flag) {
		tr.remove();
		var nextTr = parTr.next('tr');
		if (1 == nextTr.length) {
			var chiVal = nextTr.find(">td input[type='hidden']").val();
			if (chiVal.indexOf(fullName) == 0
					&& 1 == compareCount(fullName, chiVal)) {
				flag = false;
			}
		}
		if (flag) {
			parTr.remove();
		}
		
		// 判断自身属性下是否还有其他子数据,若没有，则需要将该行的样式去掉
		var selfTr = $('#selfPro').parent().parent();
		var nextAll = selfTr.nextAll();
		if (nextAll.length <= 0) {
			selfTr.find(">td div").attr('class', 'tbdiv');
			selfTr.find(">td div").find('span:last-child').css('display',
					'none');
		}
	}
// setTrWidth();
	
};
/**
 * 删除后台数据
 */
var deleteRow = function (currentFullName){
	var proTitle = currentFullName.substring(currentFullName.lastIndexOf(".")+1);
	var flag = false;
	var classTitle = $('#classTitle').val();
	var userAccount = $('#userAccount').val();
	var basePath = $("#basePath").val();
  　　var url =basePath+"servlet/DeleteServlet";  
  	$.ajax({
		type:"POST",
		url:url,
		dataType:"json",
		data:{"classTitle":encodeURI(classTitle),
			"proTitle":encodeURI(proTitle),
			"opClass":encodeURI('ResDefProperty'),
			"userAccount":encodeURI(userAccount)},
		async: false,
		success:function(returnValue)
			{
			flag =  returnValue.deletType;
			},
		error:function(){
			alert("错误");	
		}
	});
  	return flag;
}


/**
 * 新增一个属性
 * 
 * @param groupName
 *            组名称
 * @param proName
 *            属性名称
 * @param proTitle
 *            属性标题
 * @param proType
 *            属性类型
 * @param tempCheckBox
 *            属性限制
 */
var newPro = function(groupName, proName, proTitle, proType, tempCheckBox) {
	// 判断自身属性<根目录>下面是否有属性组

	// 2.增加新组
	// 2.1 判断新修改的属性组名是否已经存在，如果存在，则把此属性数据放入该组内
	// 2，2 如果不存在该属性组名，则新增一个属性组名称，并将此数据存入新增加的组名中
	var tempGroupName = '自身属性.';
	var parTr;

	var selfTr = $('#selfPro').parent().parent();
	var nextAll = selfTr.nextAll();
	if (nextAll == undefined || 0 == nextAll.length) {
		// 自身属性下没有任何元素
		// 将自身属性这一行数据的属性变为
		var div = selfTr.children().first().find('>div');
		div.attr('class', 'tbdiv');
		div.find('span:last-child').css('display', 'block');
	} else {
		nextAll.each(function(i, trNode) {
			var fullName = $(trNode).children('td:first-child').children(
					'input[type="hidden"]').val();
			if (('自身属性.' + groupName) == fullName) {
				// 要新增的属性的组名已经存在，直接在此组下追加新行即可
				parTr = $(trNode);
				return false;
			} else {
				// /
			}
		});
	}
	var tempJson = {};
	tempJson.isCompare = $(tempCheckBox[0]).is(':checked');
	tempJson.uiVisible = $(tempCheckBox[1]).is(':checked');
	tempJson.isMultLine = $(tempCheckBox[2]).is(':checked');
	tempJson.required = $(tempCheckBox[3]).is(':checked');
	tempJson.readonly = $(tempCheckBox[4]).is(':checked');
	tempJson.isSaveChange = $(tempCheckBox[5]).is(':checked');
	tempJson.isAutoUpdate = $(tempCheckBox[6]).is(':checked');

	tempJson.propertyDescr = proName;
	tempJson.propertyTitle = proTitle;
	tempJson.propertyType = proType;
	tempJson.note = $('#note').val();

	tempJson.defaultValue = $('#defaultVal').val();
	tempJson.storageField = $('#storageField').val();
	tempJson.dimName = $('#dimName').val();
	tempJson.storageDatatype = $('#storageDatatype').attr('name') + '('
			+ $('#storageDatatype').val() + ')';

	// 没有找到存在的属性组名称
	if (parTr == undefined || parTr == null) {
		var dataJson = {};
		var jsonArray = [];
		dataJson.groupName = groupName;
		jsonArray.push(tempJson);
		dataJson.subMenu = jsonArray;
		// alert(JSON.stringify(dataJson));
		builderSecondMenu('自身属性', dataJson, 1, false);
		var div = $('#selfPro').next()[0];
		$(div).attr('class', 'tbdiv');
		openOrCloseSecondMenu(div);
	} else {
		// 存在属性组，追加即可
		var div = $('#selfPro').next()[0];
		var clsName = $(div).attr('class');
		var div2 = parTr.find(">td div");
		var parClsName = div2.attr('class');
		$(div).attr('class', 'tbdiv');
		openOrCloseSecondMenu(div);
			// 代开二级菜单
		if(parClsName == 'tbdiv') {
			openOrCloseSecondMenu(div2[0]);
		}
		builderThirdMenuMenu(parTr, '自身属性.' + groupName, tempJson, 2, true);
// openOrCloseSecondMenu(div);
// openOrCloseSecondMenu(div);
// var div2 = parTr.find(">td div").attr('class', 'tbdiv');
// openOrCloseSecondMenu(div2);
	}
	
// setTrWidth();
};
/**
 * 比较str2中某个字母是否比str1中某个字母出现的次数大一
 * 
 * @param str1
 *            父标题
 * @param str2
 *            子标题
 * @returns {number} 1 大一 0 不大于1
 */
var compareCount = function(str1, str2) {
	var chilCount = str_repeat(str2);
	var parCount = str_repeat(str1);
	if (parCount + 1 == chilCount)
		return 1;
	else
		return 0;
}
/**
 * 统计字符串中某个字母出现的次数
 * 
 * @param str
 * @returns {number}
 */
var str_repeat = function(str) {
	var sub = ".";
	var k = 0, sum = 0;
	k = str.indexOf(sub);
	while (k > -1) {
		sum += 1;
		k = str.indexOf(sub, k + 1);
	}
	return sum;
};
/**
 * 修改属性组名称
 * 
 * @param data
 */
var updateGroupName = function(data) {
	var currentData = $(data);
	var span = currentData.parent().prev(); // 保存组名称的地方
	var tempVal = span.html();
	span.empty();
	span
			.append('<input class="tbinpt" type="text" style="width:80px;"  value="'
					+ tempVal
					+ '"onclick = "event.cancelBubble=true;" onblur="checkVal(this)"/>');
	span.children('input').focus().val(tempVal);
	currentData.parent().empty();
};
/**
 * 修改组名检查
 * 
 * @param data
 */
var checkVal = function(data) {
	var currentData = $(data).parent();
	var temp = currentData.parent().prev().val(); // 旧的组名
	var oldVal = temp.substring(temp.indexOf('.') + 1);
	var newVal = $(data).val().replace(/(^\s*)|(\s*$)/g, '');
	var flag = false;
	if(newVal == ''||(oldVal == newVal)) {
		// 没有做任何修改
		currentData.html(oldVal);
		currentData
		.next()
		.html(
		'<a href="javascript:void(0);" class="tbedit"  onclick="updateGroupName(this);event.cancelBubble=true;"></a>');
	} else if(newVal.length>15){
    	alert("属性组名称不能多于15个！");
    	return ;
    } else {
		// 需要同步到后台数据库
// currentData.html(newVal);
		var updateProTitle = [];
		var tr = currentData.parent().parent().parent();
		tr.nextAll('tr').each(
				function(i, trNode) {
					var nexVal = $(trNode).find(">td input[type='hidden']")
							.val();
					if (0 == nexVal.indexOf(temp + '.')
							&& 1 == compareCount(temp, nexVal)) {
						nexVal = nexVal.substring(nexVal.lastIndexOf('.') + 1);
						updateProTitle.push(nexVal);
						/*
						 * $(trNode).find(">td input[type='hidden']").val(
						 * '自身属性.' + newVal + '.' + nexVal.substring(nexVal
						 * .lastIndexOf('.')));
						 */
					} else {
						return false;
					}
				});
// currentData.parent().prev().val(
// temp.substring(0, temp.indexOf('.')) + '.' + newVal);
		// 将这个json传递到后台保存，
		flag = updateProGroupName(updateProTitle.join(","),newVal);
		if(flag){
			refushProperties(newVal);
		}else{
			refushProperties(oldVal);
		}
	}
	
};

/**
 * @param updateProTitle
 *            要修改的属性的数组
 * @param newVal
 *            要修改的属性的属性组名
 * 
 * @returns
 */
var updateProGroupName = function (updateProTitle,newVal){
	var flag = false;
	var basePath = $("#basePath").val();
	var classTitle = $('#classTitle').val();
	var note = $('#note').val();
	var userAccount = $('#userAccount').val();
	var url = basePath + "servlet/UpdateClassServlet";
	$.ajax({
		type : "POST",
		url : url,
		dataType : "json",
		data : {
			"classTitle":encodeURI(classTitle),
			"opClass":encodeURI("ResDefProperty"),
			"optionType":encodeURI("updateGroup"),
			"updatePro":encodeURI("propertyGroup"),
			"updateVal":encodeURI(newVal),
			"proTitle" : encodeURI(updateProTitle),
			"userAccount":encodeURI(userAccount)
		},
		async : false,
		success : function(returnValue) {
			flag = returnValue.newOrupdate;
		},
		error : function() {
			alert("错误");
		}
	});
	return flag;
};
/**
 * 获取css通用的路径
 * 
 * @returns
 */
function getCSSPath() {
	var cssPath = $('#cssPath').val();
	return cssPath;
};
/**
 * 属性约束维护
 * 
 * @param data
 * @returns
 */
var proConstraint = function(data) {
	var basePath = $("#basePath").val();
	$("#currProTitle").val(
			$(data).parent().parent().children('td').eq('1').html());
	var proType = transformationFromStrToNum($(data).parent().parent().children('td').eq('3').html());
	if(proType == '1' || proType ==  1){
		alert("布尔类型不可以添加属性约束！");
		return false;
	}else if(proType == '9' || proType == 9){
		var dimName = $(data).parent().next().children().eq('1').val();
		var objId = dimName.substring(dimName.indexOf('(')+1,dimName.indexOf(')'));
		$('#belObj').val(objId);
	}
	$('#propertyType').val(proType);
	
	
	var form = document.getElementById("updateConstraint");
	form.action = basePath + "modelManager/constraintModel.jsp";
	form.method = 'POST';
	form.target = '_self';
	form.submit();

};
/**
 * 初始化所属对象
 * 
 * @param searchData
 * @param id
 * @returns
 */
var initRel_pro = function(searchData, id) {
	// var searchData = eval('('+returnValue+")");
	// var searchData =
	// ["状态(phase)-$-zhuangtai","父对象(parentResID)-$-fuduixiang","创建人员(createdAccount)-$-chuangjianrenyuan","资源类型(resCategory)-$-ziyuanleixing","CI配置管理员(resCiAdmin)-$-CIpeizhiguanliyuan","关键级别(priority)-$-guanjianjibie","最后修改人员(lastUpdatedAccount)-$-zuihouxiugairenyuan"];
	// var searchData = 'caocao,liubei,guanyu';
	$("#" + id).typeahead({
		source : searchData, // 绑定数据源
		highlighter : function(item) {
			return item.split("-\$-")[0];
		},
		updater : function(item) {
			return item.split("-\$-")[0];
		},
		afterSelect : function(){
			/*
			 * var temp = item.split("-\$-")[0]; var classTitle =
			 * temp.substring(temp.indexOf('(')+1,temp.indexOf(')'));
			 * getAllDefaltVal(classTitle);
			 */
		},
		hide: function () {
		}
	});
	// $("#searchTree").val('caocao');
};

/**
 * 
 * @param data
 * @param defaultVal
 *            默认值
 * @param defaultDimName
 *            所属对象默认值
 *            
 *@param opType 操作符
 * @returns
 */
var checkChangeType = function(data,defaultVal,defaultDimName,opType) {
	var val = $(data).val();
	
	$('#defaultTd').empty();
	$('#dimName').attr("disabled", true).val(defaultDimName);
	var returnVal = intitDefaultVal(val,defaultVal);
	
	$('#defaultTd').append('<input class="xjinp" name="'+returnVal.name+'" type="text" id="defaultVal" hint="默认值" allownull="false" style="color:#999"/>');
	$('#defaultVal').val(returnVal.defaultVal);
	
	if (val == '8' || val == 8 || val == '10' || val == 10 || val == '11'
			|| val == 11) {
		// 库表字段长度变化
		$('#storageDatatype').attr('name', 'varchar').val('190');

		// $('#storageDatatype_id').empty();
		// $('#storageDatatype_id').append('varchar(<input type="text"
		// id="storageDatatype" value="190"/>)');
	} else {
		$('#storageDatatype').attr('name', 'numeric').val('20');

		// $('#storageDatatype_id').empty();
		// $('#storageDatatype_id').append('numeric(<input type="text"
		// id="storageDatatype" value="20"/>)');
	}
	defaultValEvent(val);
	if((val == '9' || val == 9) && opType=='update'){
		$('#defaultVal').click();
	}
};

/**
 * 默认值绑定事件
 */
var defaultValEvent = function (val){
	$('#defaultVal').click(function(){
		var name = $(this).attr('name');
		if(this.value.replace(/(^\s*)|(\s*$)/g, "")==name){
			this.value='';
			this.style.color='#000';
		}
		if(val == '9' || val == 9 ){
			var dimName = $('#dimName').val().replace(/(^\s*)|(\s*$)/g, "");
			var modeData = eval('(' + $('#modeData').val() + ")");;
			if(!checkBootModel(dimName,modeData)){
				$('#defaultTr').addClass("errortd");
				$('#defaultTr').find('td:last-child').find('div')
						.html("请先正确填写对应对象！");
				return ;
			}
			var temp = dimName.split("-\$-")[0]; 
			var classTitle = temp.substring(temp.indexOf('(')+1,temp.indexOf(')'));
			getAllDefaltVal(classTitle);
		}
	});
	$('#defaultVal').blur(function (){
		$('#defaultTr').removeClass();
		var name = $(this).attr('name');
		if(this.value.replace(/(^\s*)|(\s*$)/g, "")==''){
			this.value=name; 
			this.style.color='#999';
		}
	});
}

/**
 * 自定填充数据库表字段
 */
var autoFillTableName = function(data) {
	var classTitle = $(data).val();
	$('#storageField').val(classTitle.toUpperCase());
};

var setIframeHeight = function (){
	 IFrameResizeByHeight("520px");
}

/**
 * 设置窗体高度
 * 
 * 
 */
var setTrWidth = function (){
//	var tempHeight =0;
	var browser = $('#browser').height()+65;
	if(browser < 600){
		browser = 600;
	}
	IFrameResizeByHeight(browser+"px");
};
/**
 * 返回最大值
 */
var getMax_val = function (val1,val2){
	return (val1>val2)?val1:val2;
};
/**
 * 获取默认值
 */
var getAllDefaltVal = function (classTitle){
	var basePath = $("#basePath").val();
  　　var url =basePath+"servlet/ProperAndConsModelServlet";  
  	$.ajax({
		type:"POST",
		url:url,
		dataType:"json",
		data:{"opType":encodeURI("getDefaltVal"),
			"classTitle":encodeURI(classTitle)},
		async: false,
		success:function(returnValue)
			{
				if(returnValue.length == 0){
					$('#defaultTr').addClass("errortd");
					$('#defaultTr').find('td:last-child').find('div')
							.html("所选对应对象没有资源！");
					return;
				}
				$('#defaultModel').val(JSON.stringify(returnValue));
				initRel_pro(returnValue,'defaultVal');
			},
		error:function(){
			alert("错误");	
		}
	})
}


