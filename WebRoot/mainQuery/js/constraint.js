/**
 * Created by huipu on 2015/10/12.
 */

$(function () {
	refushConstraints();
	var proType = $('#propertyType').val();
	   
    initConstraintType(proType);
});

/**
 * 刷新属性约束
 * @returns
 */
var refushConstraints = function (){
	$('#con_table').empty();
	var classTitle = $('#classTitle').val();
	var proTitle = $('#proTitle').val();
	var basePath = $("#basePath").val();
  　　var url =basePath+"servlet/ProperAndConsModelServlet";  
  	$.ajax({
		type:"POST",
		url:url,
		dataType:"text",
		data:{"opType":encodeURI("constraintModle"),
			"classTitle":encodeURI(classTitle),
			"propertyTitle":encodeURI(proTitle)},
		async: false,
		success:function(returnValue)
			{
			$("#dataJson").val(returnValue);
			builderTable();
			setTrWidth();
			},
		error:function(){
			alert("错误");	
		}
	})
	
}



/**
 * 建立table表格
 */
var builderTable = function (){
	 var table = $('#con_table');
	 var firstTr = '<tr><th scope="col" class="cons_name" style="display: none">类名</th><th scope="col" class="pro_name" style="display: none">属性名</th><th class="cons_type" scope="col">约束类型</th><th class="min_val" scope="col">最小值</th><th class="max_val" scope="col">最大值</th><th class="zhengze" scope="col">正则表达式</th><th class="ext_class" scope="col">外部类</th><th class="guanlian_pro" scope="col">关联属性</th><th class="yinyong_pro" scope="col">引用属性</th><th class="tda" scope="col">操作</th><th class="disp" style="display: none">隐藏属性</th></tr>';
	 table.append(firstTr);
	 
	    var data = eval('('+$('#dataJson').val()+')');
	    $("#classTitle").val(data.classTitle);
	    $("#className").val(data.className);
	    $("#proTitle").val(data.proTitle);
	    $("#proName").val(data.proName);
	    $("#proType").val(data.proType);
	    var ch = data.subMenu;
	    $.each(ch, function (k, chil) {
	        var tr = '<tr><td class="cons_name" style="display: none"><input type ="hidden"  value="' + data.classTitle + '_' + data.proTitle + '"/>' + data.className + '</td><td  class="pro_name"  style="display: none">' + data.proName + '</td><td  class="cons_type" >' + transformationStrFromNum(''+chil.constraintType) + '</td><td class="min_val" >' + chil.minValue + '</td><td  class="max_val" >' + chil.maxValue + '</td><td   class="zhengze" >' + chil.rexStr + '</td><td class="ext_class" >' + chil.extClass + '</td><td class="guanlian_pro">' + chil.relaProperty + '</td><td  class="yinyong_pro">' + chil.relaRelation + '</td><td class="tda"><a href="javascript:void(0);" onclick="proConstraint(this)">修改</a><a href="javascript:void(0);" onclick="deletConstraint(this)">删除</a></td><td  class="disp" style="display: none"><input type ="hidden" value="'+chil.constraintName+'"/></td></tr>';
	        table.append(tr);
	    });
	    
}


/**
 * 维护属性约束
 * 
 * @param data
 */
var proConstraint = function (data) {
	 $('#cons_type').attr("disabled", false);
	 	removeAllClass();
	    document.getElementById('light').style.display = 'block';
	    document.getElementById('fade').style.display = 'block';
	    var divLi = document.getElementById('light');
	    $(divLi).height(275);
	    var divTop = getHeight();
	    $(divLi).css("top",(divTop-137)+'px');
	    
    if ($(data).length == 0) {
        // 新增
    	$('#opType').val('add');
        $('#min_val').attr("disabled", true).val('');
        $('#max_val').attr("disabled", true).val('');
        $('#reg_exp').attr("disabled", true).val('');
        $('#ext_cls').attr("disabled", true).val('');
        $('#rel_pro').attr("disabled", true).val('');
        $('#quote_pro').attr("disabled", true).val('');
        var text = $("#cons_type").find("option").eq(0).text();
        initSele(text);
        consTypeChange($("#cons_type")[0]);
    } else {
        var tr = $(data).parent().parent();
        var classAndPro = tr.find('td input[type="hidden"]').val();
        $('#opType').val('update');
        initSele(tr.find('td').eq('2').html());
        $('#cons_type').attr("disabled", true);
        var num = transformationNumFromStr(tr.find('td').eq('2').html())+"";
        initDispl(num);
        $('#min_val').val(tr.children('td').eq('3').html());
        $('#max_val').val(tr.children('td').eq('4').html());
        $('#ext_cls').val(tr.children('td').eq('6').html());
        $('#rel_pro').val(tr.children('td').eq('7').html());
        $('#quote_pro').val(tr.children('td').eq('8').html());
        $('#reg_exp').val(tr.children('td').eq('5').html());
// consTypeChange($('#cons_type')[0]);
    }
    var propertyType = $('#propertyType').val();
    var propettyTitle = $('#proTitle').val();
    if(propertyType == 9 || propertyType =='9'){
    	var objId = $("#belObj").val();
    	getRel_pro( $("#classTitle").val(),propertyType,objId,propettyTitle);
    }
    // 点击确定按钮
    $('#con_sub').bind('click', function () {
    	removeAllClass();
        var flag = submitConstaintMaintain(data);
        if(flag){
        	document.getElementById('light').style.display = 'none';
        	document.getElementById('fade').style.display = 'none';
        	$(this).unbind('click');
            //刷新
            refushConstraints();
        }
    });

    $('#con_exi').bind('click', function () {
        document.getElementById('light').style.display = 'none';
        document.getElementById('fade').style.display = 'none';
        $('#con_sub').unbind('click');
    });
};

/**
 * 遮罩层高都
 * @returns
 */
function getHeight(){
	var liulnaqiHei =  (parent.browerHei)/2; //浏览器可视化高度
    var scroHei = $(window.parent).scrollTop();  //滚动条高度
//    var parh = 52;
   return scroHei+liulnaqiHei+43;
}

/**
 * 移除所有的错误提示样式
 * 
 * @returns
 */
var removeAllClass = function (){
	$('#constraintTypTr').removeClass();
	$('#cons_min').removeClass();
    $('#cons_max').removeClass();
    $('#cons_reg').removeClass();
    $('#cons_ext').removeClass();
    $('#cons_rel').removeClass();
    $('#quote_rel').removeClass();
};
/**
 * 从后台获取该类名下特定类型属性的集合
 * 
 * @param classTitle
 *            类标题
 * @param proType
 *            属性类型
 * @param objId
 *            所属对象ID   
 * @param propettyTitle
 *            要建立的属性  
 *                   
 */
var getRel_pro = function(classTitle,proType,objId,propettyTitle){
	var basePath = $("#basePath").val();
	var url = basePath + "servlet/ProGetServlet";
	var userAccount = $('#userAccount').val();
	$.ajax({
		type : "POST",
		url : url,
		data : {
			"classTitle" : encodeURI(classTitle),
			"proType" : encodeURI(proType),
			"userAccount":encodeURI(userAccount),
			"objId":encodeURI(objId),
			"propettyTitle":encodeURI(propettyTitle)
		},
		dataType : "json",
		async : false,
		success : function(returnValue) {
			$('#modelData').val(JSON.stringify(returnValue));
			initRel_pro(returnValue);
		},
		error : function() {
			alert("错误");
		}
	});
	
};
/**
 * 初始化关联属性输入框
 * 
 * @param returnValue
 * @returns
 */
var initRel_pro = function (searchData) {
  var rel_pro = searchData.rel_pro;
  var quo_pro = searchData.quo_pro;
  
  $("#rel_pro").typeahead({
  source: rel_pro,  // 绑定数据源
  highlighter: function (item) {
      return item.split("-\$-")[0];
  },
  updater: function (item) {
      return item.split("-\$-")[0];
  },
  afterSelect: function (item) {
  }
});
  $("#quote_pro").typeahead({
	  source: quo_pro,  // 绑定数据源
	  highlighter: function (item) {
	      return item.split("-\$-")[0];
	  },
	  updater: function (item) {
	      return item.split("-\$-")[0];
	  },
	  afterSelect: function (item) {
	  }
	});
  
};
/**
 * 提交属性约束维护修改
 */
var submitConstaintMaintain = function(data){
	var opType = $('#opType').val();
	 var conType = $('#cons_type').val();
	 var flag = false;
	 var tr ;
	 flag = checkInputFormat(opType);
	 if(flag){
		 return false;
	 }
	if(opType == "update"){
		tr = $(data).parent().parent();
	}
    var min_val = $('#min_val').val();
    var max_val = $('#max_val').val();
    var reg_exp = $('#reg_exp').val();
    var ext_cls = $('#ext_cls').val();
    
//    var modelData = eval('('+$('#modelData').val()+')');
//    var rel_pro = getBootModel($('#rel_pro').val(),modelData.rel_pro);
    var rel_pro = $('#rel_pro').val();
    var quote_pro =$('#quote_pro').val();
    var propertyType = $('#propertyType').val();
    var constraintType = $('#cons_type').val()+'';
    
    var classTitle = $("#classTitle").val();
    var proTitle = $("#proTitle").val();
    var className = $("#className").val();
    var proName = $("#proName").val();
    var oldConsType = "";
    var constraintName = "";
    if(opType == "update"){
    	oldConsType = transformationNumFromStr(tr.children('td').eq('2').html());
    	constraintName = $(data).parent().next().find("input[type='hidden']").val();
    }
    var result  = saveOrUpdate(opType,oldConsType,constraintName,classTitle,proTitle,constraintType,min_val,max_val,reg_exp,ext_cls,rel_pro,quote_pro,propertyType);
    var isSave = result.newOrupdate;
//    var resourceId = result.resourceId;
    return isSave;
};

/**
 * 获取引用属性或关联属性的resID
 */
var getBootModel = function (modelName,modeData){
	var endStr = '';
    $.each(modeData, function(i, tempData) {
        var temp1 = tempData.split('-\$-');
        if(modelName == temp1[0]){
        	endStr = temp1[1];
            return false;
        }
    });
    return endStr;
};
/**
 * 检查非空
 * 
 * @param opType
 * @returns
 */
var checkInputFormat = function(opType){
	var flag = false;
	var conType = $('#cons_type').val();
	if(opType == "add"){
		// 新增
		 $('#con_table').find('tr').each(function(i,tempTr){
		        var tempConType = $(tempTr).children('td').eq('2').html();
		        tempConType = transformationNumFromStr(tempConType)+'';
		        if(tempConType == conType){
		        	$('#constraintTypTr').addClass("errortd");
					$('#constraintTypTr').find('td:last-child').find('div')
							.html("此约束已存在！");
		            flag = true;
		            return false;
		        }
		    });
	}
	if(flag){
		// 重复，不在执行
		return flag;
	}
	flag = false;
	if(conType == 2 || conType == '2'){
		var min_val = $('#min_val').val().replace(/(^\s*)|(\s*$)/g, "");
		var max_val = $('#max_val').val().replace(/(^\s*)|(\s*$)/g, "");
		if(min_val == "" || min_val.length > 12){
			$('#cons_min').addClass("errortd");
			$('#cons_min').find('td:last-child').find('div')
					.html("长度范围1-12个！");
			flag = true;
		}
		if(max_val == "" || max_val.length > 12){
			$('#cons_max').addClass("errortd");
			$('#cons_max').find('td:last-child').find('div')
					.html("长度范围1-12个！");
			flag = true;
		}
		if(flag){
			return flag;
		}
		var proType = $('#propertyType').val();
		if(proType == 2 || proType == '2' || proType == 3 || proType == '3'){
			//整型
			if (! /^(-)?\d+$/.test(min_val)) {
				//不全是数字
				$('#cons_min').addClass("errortd");
				$('#cons_min').find('td:last-child').find('div')
						.html("必须输入整数！！");
				flag = true;
				
			}
			if (! /^(-)?\d+$/.test(max_val)) {
				//不全是数字
				$('#cons_max').addClass("errortd");
				$('#cons_max').find('td:last-child').find('div')
						.html("必须输入整数！！");
				flag = true;
				
			}
		}else if(proType == 4 || proType == '4'){
			//浮点型
			if(!/^(-)?\d+(\.\d+)?$/.test(min_val)){
				$('#cons_min').addClass("errortd");
				$('#cons_min').find('td:last-child').find('div')
						.html("必须输入数字！！");
				flag = true;
			}
			if(!/^(-)?\d+(\.\d+)?$/.test(max_val)){
				$('#cons_max').addClass("errortd");
				$('#cons_max').find('td:last-child').find('div')
						.html("必须输入数字！！");
				flag = true;
			}
		}
		if(flag){
			return flag;
		}
// var temp1 = $('#min_val').val();
        min_val = parseFloat(min_val);
// var temp2 = $('#max_val').val();
        max_val = parseFloat(max_val);
		
		if(min_val !="" && max_val !="" && min_val>=max_val){
			$('#cons_min').addClass("errortd");
			$('#cons_min').find('td:last-child').find('div')
					.html("最小值必须小于最大值！！");
			flag = true;
		}
	}
	if(conType == 3 || conType == '3'|| conType == 4 || conType == '4'){
		var reg_exp = $('#reg_exp').val().replace(/(^\s*)|(\s*$)/g, "");
		if(reg_exp == ""){
			$('#cons_reg').addClass("errortd");
			$('#cons_reg').find('td:last-child').find('div')
					.html("必须输入值！");
			flag = true;
		}
	}
	if(conType == 6 || conType == '6' || conType == 8 || conType == '8'){
		var ext_cls = $('#ext_cls').val().replace(/(^\s*)|(\s*$)/g, "");
		if(ext_cls == ""){
			$('#cons_ext').addClass("errortd");
			$('#cons_ext').find('td:last-child').find('div')
					.html("必须输入外部类！");
			flag = true;
		}
	}
	if(conType == 7 || conType == '7'){
		var rel_pro = $('#rel_pro').val().replace(/(^\s*)|(\s*$)/g, "");
		var quote_pro = $('#quote_pro').val().replace(/(^\s*)|(\s*$)/g, "");
		if(rel_pro == ""){
			$('#cons_rel').addClass("errortd");
			$('#cons_rel').find('td:last-child').find('div')
					.html("必须输入关联属性！");
			flag = true;
		}
		if(quote_pro == ""){
			$('#quote_rel').addClass("errortd");
			$('#quote_rel').find('td:last-child').find('div')
					.html("必须输入引用属性！");
			flag = true;
		}
		if(flag){
			return true;
		}
		var modelData = eval('('+$('#modelData').val()+')');
		if(!checkBootModel(rel_pro,modelData.rel_pro)){
			// 格式不正确
			alert("请正确输入关联属性！");
			return true;
		}
		if(!checkBootModel(quote_pro,modelData.quo_pro)){
			// 格式不正确
			alert("请正确输入引用属性！");
			return true;
		}
	}
	return flag;
};
/**
 * 判断数组中是否包含输入的值
 * 
 * @param modelName
 *            输入的值
 * @param modeData
 *            数组对象
 * @returns {boolean}
 */
var checkBootModel = function (modelName,modeData){
    var  flag = false;
    // modeData 格式如下：
    // ["状态(phase)-$-zhuangtai","父对象(parentResID)-$-fuduixiang","创建人员(createdAccount)-$-chuangjianrenyuan","资源类型(resCategory)-$-ziyuanleixing","CI配置管理员(resCiAdmin)-$-CIpeizhiguanliyuan","关键级别(priority)-$-guanjianjibie","最后修改人员(lastUpdatedAccount)-$-zuihouxiugairenyuan"];
   ;
    $.each(modeData, function(i, tempData) {
        var temp1 = tempData.split('-\$-');
        if(modelName == temp1[0]){
            flag = true;
            return false;
        }
    });
    return flag;
};
/**
 * 新增或修改属性约束
 * 
 * @param opType
 *            操作类型
 * @param classTitle
 * @param proTitle
 * @param constraintType
 * @param min_val
 * @param max_val
 * @param reg_exp
 * @param ext_cls
 * @param rel_pro
 * @param quote_pro
 * @param propertyType
 * @returns
 */
var saveOrUpdate = function (opType,oldConsType,constraintName,classTitle,proTitle,constraintType,min_val,max_val,reg_exp,ext_cls,rel_pro,quote_pro,propertyType){
	var flag ;
	var basePath = $("#basePath").val();
	var url = basePath + "servlet/UpdateClassServlet";
	var userAccount = $('#userAccount').val();
	$.ajax({
		type : "POST",
		url : url,
		dataType : "json",
		data : {
			"opClass":encodeURI("ResDefConstraint"),
			"constraintName":encodeURI(constraintName),
			"oldConsType":encodeURI(oldConsType),
			"optionType" : encodeURI(opType),
			"classTitle" : encodeURI(classTitle),
			"proTitle" : encodeURI(proTitle),
			"propertyType" : encodeURI(propertyType),
			"min_val" : encodeURI(min_val),
			"max_val" : encodeURI(max_val),
			"reg_exp" : encodeURI(reg_exp),
			"ext_cls" : encodeURI(ext_cls),
			"rel_pro" : encodeURI(rel_pro),
			"quote_pro" : encodeURI(quote_pro),
			"constraintType" : encodeURI(constraintType),
			"userAccount":encodeURI(userAccount)
		},
		async : false,
		success : function(returnValue) {
			flag = returnValue;
		},
		error : function() {
			alert("错误");
		}
	});
	return flag;
	
};


/**
 * 初始化属性约束维护的属性显隐性
 * 
 * @param num
 */
var initDispl = function (num) {
    switch (num) {
        case '1':
        case '5':
            // 全部不可编辑
            updatDisplayClass([true, true, true, true, true, true]);
            return;
        case '2':
            updatDisplayClass([false, false, true, true, true, true]);
            return;
        case '3':
        case '4':
            updatDisplayClass([true, true, false, true, true, true]);
            return;

        case '6':
            updatDisplayClass([true, true, true, false, true, true]);
            return;
        case '7':
            updatDisplayClass([true, true, true, true, false, false]);
            
            return;
        case '8':
            updatDisplayClass([true, true, true, false, true, true]);
            return;
    }
};
/**
 * 决定显隐性
 * 
 * @param str
 */
var updatDisplayClass = function (str) {
    $('#min_val').attr("disabled", str[0]).val("");
    $('#max_val').attr("disabled", str[1]).val("");
    $('#reg_exp').attr("disabled", str[2]).val("");
    $('#ext_cls').attr("disabled", str[3]).val("");
    $('#rel_pro').attr("disabled", str[4]).val("");
    $('#quote_pro').attr("disabled", str[5]).val("");
};
/**
 * 初始化下拉
 * 
 * @param num
 */
var initSele = function (str) {
    var num = transformationNumFromStr(str);
    $("#cons_type").val(num);
// $('#cons_type option').each(function (i, option) {
// var val = $(option).val();
// if (val == num)
// $(option).attr('selected', true);
// });
}
/**
 * 从字符串转为数字类型
 * 
 * @param str
 * @returns {number}
 */
var transformationNumFromStr = function (str) {
    switch (str) {
        case '类型约束':
            return 1;
        case '值域约束':
            return 2;
        case '格式约束':
            return 3;
        case '枚举约束':
            return 4;
        case '唯一约束':
            return 5;
        case '外部类':
            return 6;
        case '属性关联约束':
            return 7;
        case '属性组合':
            return 8;
        default :
            return 0;
    }
};

/**
 * 将数字类型转为字符串类型
 * 
 * @param num
 * @returns {*}
 */
var transformationStrFromNum = function (num) {
    switch (num) {
        case '1':
            return '类型约束';
        case '2':
            return '值域约束';
        case '3':
            return '格式约束';
        case '4':
            return '枚举约束';
        case '5':
            return '唯一约束';
        case '6':
            return '外部类';
        case '7':
            return '属性关联约束';
        case '8':
            return '属性组合';
    }
};
/**
 * 下拉选项改变
 * 
 * @param data
 */
var consTypeChange = function (data) {
    var val = $(data).val()
    initDispl(val);
// if(val == 7 || val =='7'){
// getRel_pro( $("#classTitle").val(),'9');
// }
};
/**
 * 初始化属性约束类型
 */
var initConstraintType = function(proType){
    // var proType = $("#proType").val();
    switch (proType){
        case '1':
            // 布尔型不能建立约束
        	$('#constraintTypTd').append('<select id="cons_type" onchange="consTypeChange(this)"><option value="6">外部类</option></select>');
            break;
        case '2':
        case '3':
        case '4':
            // 值域约束，唯一约束
            $('#constraintTypTd').append('<select id="cons_type" onchange="consTypeChange(this)"><option value="2">值域约束</option><option value="5">唯一约束</option><option value="6">外部类</option></select>');
            // return '浮点型';
            break;
        case '5':
            // 值域约束，唯一约束 ,注意值域约束输入方式变为时间输入框
            $('#constraintTypTd').append('<select id="cons_type" onchange="consTypeChange(this)"><option value="6">外部类</option></select>');
            // return '日期';
            break;
        case '6':
            $('#constraintTypTd').append('<select id="cons_type" onchange="consTypeChange(this)"><option value="6">外部类</option></select>');
            break;
        // return '时间型';
        case '7':
            $('#constraintTypTd').append('<select id="cons_type" onchange="consTypeChange(this)"><option value="6">外部类</option></select>');
            break;
        // return '日期时间型';
        case '8':
            $('#constraintTypTd').append('<select id="cons_type" onchange="consTypeChange(this)"><option value="3">格式约束</option> <option value="4">枚举约束</option><option value="5">唯一约束</option> <option value="6">外部类</option><option value="8">属性组合</option></select>');
            break;
        // 格式、枚举、唯一、外部、属性组合
        // return '字符串型';
        case '9':
            $('#constraintTypTd').append('<select id="cons_type" onchange="consTypeChange(this)"><option value="6">外部类</option><option value="7">属性关联约束</option></select>');
            break;
        // 格式、枚举、唯一、外部、属性关联、属性组合
        // return '列表型';
        case '10':
            $('#constraintTypTd').append('<select id="cons_type" onchange="consTypeChange(this)"><option value="3">格式约束</option><option value="5">唯一约束</option> <option value="6">外部类</option></select>');
            break;
        // 格式、枚举、唯一、外部、属性组合
        // return 'IP地址型';
        case '11':
            $('#constraintTypTd').append('<select id="cons_type" onchange="consTypeChange(this)"><option value="3">格式约束</option><option value="6">外部类</option></select>');
            break;
        // 格式、外部
        // return '密码型';
        default :
            $('#constraintTypTd').append('<select id="cons_type" onchange="consTypeChange(this)"><option value="1">类型约束</option> <option value="2">值域约束</option> <option value="3">格式约束</option> <option value="4">枚举约束</option> <option value="5">唯一约束</option> <option value="6">外部类</option> <option value="7">属性关联约束</option> <option value="8">属性组合</option> </select>');
    }
};
/**
 * 删除属性约束
 * 
 * @param data
 * @returns {boolean}
 */
var deletConstraint = function (data){
	var flag = confirm("确定删除吗？");
	if(!flag){
		return ;
	}
	
    var tr = $(data).parent().parent();
    var deleType = deleteRow(tr);
	if(deleType){
		refushConstraints();
	}else{
		alert("删除失败！");
	}
};
/**
 * 删除后台数据
 */
var deleteRow = function (tr){
	var consType=tr.children('td').eq('2').html();
	consType = transformationNumFromStr(consType);
	var classTitle = $('#classTitle').val();
    var proTitle = $('#proTitle').val();
	var flag = false;
	var classTitle = $('#classTitle').val();
	var constraintName = tr.children('td').eq('10').find('input[type="hidden"]').val();
	var basePath = $("#basePath").val();
  　　var url =basePath+"servlet/DeleteServlet";  
  var userAccount = $('#userAccount').val();
  	$.ajax({
		type:"POST",
		url:url,
		dataType:"json",
		data:{"classTitle":encodeURI(classTitle),
			"proTitle":encodeURI(proTitle),
			"consType":encodeURI(consType),
			"constraintName":encodeURI(constraintName),
			"opClass":encodeURI('ResDefConstraint'),
			"userAccount":encodeURI(userAccount)},
		async: false,
		success:function(returnValue)
			{
			flag =  returnValue.deletType;
			},
		error:function(){
			alert("错误");	
		}
	});
  	return flag;

};
/**
 * 设置窗体高度
 * 
 * 
 */
var setTrWidth = function (){
	var browser = $('#browser').height()+65;
	if(browser < 600){
		browser = 600;
	}
	IFrameResizeByHeight(browser+"px");
};
/**
 * 返回最大值
 */
var getMax_val = function (val1,val2){
	return (val1>val2)?val1:val2;
}