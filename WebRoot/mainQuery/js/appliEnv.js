$(function () {
	refushData();
	  $('#searchCode').bind('keypress',function(e){
		  var event = e || window.event;  
	        if (event.keyCode == 13){
	        	searchData();
	        }
	            
      });
//	trHeight();
});
/**
 * 刷新数据
 */
var refushData = function (){
	$('#env_table').empty();
	var classTitle = $('#classTitle').val();
//	var proTitle = $('#proTitle').val();
	var basePath = $("#basePath").val();
  　　var url =basePath+"servlet/ProperAndConsModelServlet";  
  	$.ajax({
		type:"POST",
		url:url,
		dataType:"text",
		data:{"opType":encodeURI("appliEnvModle"),
			"classTitle":encodeURI(classTitle)},
		async: false,
		success:function(returnValue)
			{
			$("#dataJson").val(returnValue);
			buildTable();
			setTrWidth();
			},
		error:function(){
			alert("错误");	
		}
	})
}
/**
 * 创建table
 */
var buildTable = function (){
	 var table = $('#env_table');
	var firstTr = '<tr><th scope="col" class="source_cls" width="200">源端类型名称</th><th class="source_pro" scope="col">源端属性名称</th><th class="dire" scope="col">关系方向</th><th scope="col" class="dir_cls">目的端类型名称</th><th scope="col" class="dir_pro">目的端属性名称</th><th class="link_co" scope="col">线条颜色</th><th class="app_tda" scope="col">操作</th><th class="app_dis" style="display: none">隐藏属性</th></tr>';
	table.append(firstTr);
	 var data = eval("("+$('#dataJson').val()+")");
	    $("#classTitle").val(data.classTitle);
	    $("#className").val(data.className);
	    var ch = data.subMenu;
	    $.each(ch, function (k, chil) {
	        var tr = $('<tr>');
	        tr.append('<td class="source_cls">' + chil.sourceClassName + '(' + chil.sourceClassTitle + ')' + '</td><td class="source_pro">' + chil.sourcePropertyName + '(' + chil.sourcePropertyTitle + ')' + '</td><td class="dire">' + transformationStrFromNum(chil.direction+'') + '</td><td  class="dir_cls">' + chil.destClassName + '(' + chil.destClassTitle + ')' + '</td><td  class="dir_pro">' + chil.destPropertyName + '(' + chil.destPropertyTitle + ')' + '</td>');
	        var td = $('<td class="link_co">');
	        var input = $('<input id="linkColor_' + k + '" value="' + chil.linkColor + '">');
	        td.append(input);
	        tr.append(td).append('<td class="app_tda"><a href="javascript:void(0);" onclick="newOrUpdateEnv(this)">修改</a><a href="javascript:void(0);" onclick="deletEnv_row(this)">删除</a></td><td class="app_dis" style="display: none"><input type ="hidden" value="'+chil.relationEnvID+'"/><input  type ="hidden" value="'+chil.sourceClassName + '(' + chil.sourceClassTitle + ')' + '-' + chil.sourcePropertyName + '(' + chil.sourcePropertyTitle + ')' + '-' + transformationStrFromNum(chil.direction+'') + '-' + chil.destClassName + '(' + chil.destClassTitle + ')' + '-' + chil.destPropertyName + '(' + chil.destPropertyTitle + ')"/></td>');
	        table.append(tr);
	        input.spectrum({
	            chooseText: "确定",
	            cancelText: "取消",
	            preferredFormat: "rgb",
	            showInput: true,
	            showPalette: true,
	            palette:[
	                ["#000000", "#434343", "#666666", "#999999", "#b7b7b7", "#cccccc", "#d9d9d9", "#efefef", "#f3f3f3", "#ffffff"],
	                        ["#980000", "#ff0000", "#ff9900", "#ffff00", "#00ff00", "#00ffff", "#4a86e8", "#0000ff", "#9900ff", "#ff00ff"],
	                        ["#e6b8af", "#f4cccc", "#fce5cd", "#fff2cc", "#d9ead3", "#d9ead3", "#c9daf8", "#cfe2f3", "#d9d2e9", "#ead1dc"],
	                        ["#dd7e6b", "#ea9999", "#f9cb9c", "#ffe599", "#b6d7a8", "#a2c4c9", "#a4c2f4", "#9fc5e8", "#b4a7d6", "#d5a6bd"],
	                        ["#cc4125", "#e06666", "#f6b26b", "#ffd966", "#93c47d", "#76a5af", "#6d9eeb", "#6fa8dc", "#8e7cc3", "#c27ba0"],
	                        ["#a61c00", "#cc0000", "#e69138", "#f1c232", "#6aa84f", "#45818e", "#3c78d8", "#3d85c6", "#674ea7", "#a64d79"],
	                        ["#85200c", "#990000", "#b45f06", "#bf9000", "#38761d", "#134f5c", "#1155cc", "#0b5394", "#351c75", "#741b47"],
	                        ["#5b0f00", "#660000", "#783f04", "#7f6000", "#274e13", "#0c343d", "#1c4587", "#073763", "#20124d", "#4c1130"]
	                ],
	            hide: function(tinycolor) {
	            	updateLinkWide(input[0],"linkColor");
	            }
	        });
	    });
}

/**
 * 初始化sourceClassName ,descrClassName输入框
 * 
 * @param searchData
 *            数组
 * @param id1
 * @param id2
 * @returns
 */
var initRel_pro = function (searchData, id1,id2) {
// var searchData = eval('('+returnValue+")");
// var searchData =
// ["状态(phase)-$-zhuangtai","父对象(parentResID)-$-fuduixiang","创建人员(createdAccount)-$-chuangjianrenyuan","资源类型(resCategory)-$-ziyuanleixing","CI配置管理员(resCiAdmin)-$-CIpeizhiguanliyuan","关键级别(priority)-$-guanjianjibie","最后修改人员(lastUpdatedAccount)-$-zuihouxiugairenyuan"];
// var searchData = 'caocao,liubei,guanyu';
    $("#" + id1).typeahead({
        source: searchData,  // 绑定数据源
        highlighter: function (item) {
            return item.split("-\$-")[0];
        },
        updater: function (item) {
            return item.split("-\$-")[0];
        },
        afterSelect: function (item) {
//        	updateProNameInput(item.split("-\$-")[0],id2);
        }
    });
// $("#searchTree").val('caocao');
};
/**
 * 提交适用环境维护修改
 * 
 * @param data
 *            当前操作的对象
 * @param opType
 *            当前操作类型
 * 
 */
var submitConstaintMaintain = function (data,opType) {
    var sourceClsName = $('#sourceClsName').val();
    var sourceProName = $('#sourceProName').val();
    var direction = $('#rel_dir').val();
    var descClsName = $('#descClsName').val();
    var descProName = $('#descProName').val();
    var linkColor = $('#linkColor').val().replace(/\s/g, "");
    var flag = false;
    
    var count  = $('#env_table').find('tr').length-1;
    $('#env_table').find('tr').each(function (i, tempTr) {
        var tempSourceClsNamee = $(tempTr).find('td').eq('0').html();
        var tempSourceProName = $(tempTr).find('td').eq('1').html();
        var tempDirection = transformationNumFromStr($(tempTr).find('td').eq('2').html());
        var tempDescClsName = $(tempTr).find('td').eq('3').html();
        var tempDescProName = $(tempTr).find('td').eq('4').html();
        var tempColor = $(tempTr).find('td').eq('5').find('input').eq(0).val();
        if(tempColor == linkColor && tempSourceClsNamee == sourceClsName && tempSourceProName == sourceProName && direction==tempDirection && tempDescClsName == descClsName && tempDescProName == descProName ){
            // 相同的适用环境
            flag = true;
            return false;
        }
    });

    if (flag) {
        alert("您没有做任何修改或该适用环境已经存在，不允许重复保存！");
        return false;
    }
     var result = updateTr(opType,data,sourceClsName,sourceProName,direction,descClsName,descProName,linkColor);
     flag = result.newOrupdate;
     var resourceId = result.resourceId;
     if(flag){
    	 refushData();
     }
    return flag;
};
/**
 * 修改适用环境
 * 
 * @param optionType
 * @param data
 * @param tempSourceClsNamee
 * @param tempSourceProName
 * @param direction
 * @param tempDescClsName
 * @param tempDescProName
 * @param linkColor
 */
var updateTr = function(optionType,data,tempSourceClsNamee,tempSourceProName,direction,tempDescClsName,tempDescProName,linkColor){
	var relationEnvID =''; 
	if($(data).length == 1 ){
		var  ddd = $(data).parent().next();
		var add = ddd.find("input[type='hidden']");
		relationEnvID = add.val();
	}
	var flag ;
	var classTitle = $('#classTitle').val();
	var basePath = $("#basePath").val();
	var url = basePath + "servlet/UpdateClassServlet";
	var userAccount = $('#userAccount').val();
	$.ajax({
		type : "POST",
		url : url,
		dataType : "json",
		data : {
			"optionType" : encodeURI(optionType),
			"opClass" : encodeURI("ResDefExtRelationEnv"),
			"classTitle" : encodeURI(classTitle),
			"relationEnvID" : encodeURI(relationEnvID),
			"sourceClass" : encodeURI(tempSourceClsNamee),
			"sourceProperty" : encodeURI(tempSourceProName),
			"destClass" : encodeURI(tempDescClsName),
			"destProperty" : encodeURI(tempDescProName),
			"direction" : encodeURI(direction),
			"linkColor" : encodeURI(linkColor),
			"userAccount":encodeURI(userAccount)
		},
		async : false,
		success : function(returnValue) {
			flag = returnValue;
		},
		error : function() {
			alert("错误");
		}
	});
	
	
	return flag;
	
};


/**
 * 追加新的适用环境
 * 
 * @param sourceClsName
 * @param sourceProName
 * @param direction
 * @param descClsName
 * @param descProName
 * @param linkColor
 * @returns
 */
var appendTr = function(sourceClsName,sourceProName,direction,descClsName,descProName,linkColor,resourceId){
	var count = getTrCount();
	 var tr = $('<tr>');
	    tr.append('<td>' + sourceClsName + '</td><td>' + sourceProName + '</td><td>' + transformationStrFromNum(direction) + '</td><td>' + descClsName + '</td><td>' + descProName + '</td>');
	    var td = $('<td>');
	    var input = $('<input id="linkColor_' + count + '" value="' + linkColor + '">');
	    td.append(input);
	    tr.append(td).append('<td class="tda"><a href="javascript:void(0);" onclick="newOrUpdateEnv(this)">修改</a><a href="javascript:void(0);" onclick="deletEnv_row(this)">删除</a></td><td style="display: none"><input type ="hidden" value="'+resourceId+'"/><input type ="hidden" value="'+sourceClsName + '-' + sourceProName + '-' + transformationStrFromNum(direction) + '-' + descClsName + '-' + descProName+'"/></td>');
	    $('#env_table').append(tr);
	    input.spectrum({
            chooseText: "确定",
            cancelText: "取消",
            preferredFormat: "rgb",
            showInput: true,
            showPalette: true,
            palette:[
                ["#000000", "#434343", "#666666", "#999999", "#b7b7b7", "#cccccc", "#d9d9d9", "#efefef", "#f3f3f3", "#ffffff"],
                        ["#980000", "#ff0000", "#ff9900", "#ffff00", "#00ff00", "#00ffff", "#4a86e8", "#0000ff", "#9900ff", "#ff00ff"],
                        ["#e6b8af", "#f4cccc", "#fce5cd", "#fff2cc", "#d9ead3", "#d9ead3", "#c9daf8", "#cfe2f3", "#d9d2e9", "#ead1dc"],
                        ["#dd7e6b", "#ea9999", "#f9cb9c", "#ffe599", "#b6d7a8", "#a2c4c9", "#a4c2f4", "#9fc5e8", "#b4a7d6", "#d5a6bd"],
                        ["#cc4125", "#e06666", "#f6b26b", "#ffd966", "#93c47d", "#76a5af", "#6d9eeb", "#6fa8dc", "#8e7cc3", "#c27ba0"],
                        ["#a61c00", "#cc0000", "#e69138", "#f1c232", "#6aa84f", "#45818e", "#3c78d8", "#3d85c6", "#674ea7", "#a64d79"],
                        ["#85200c", "#990000", "#b45f06", "#bf9000", "#38761d", "#134f5c", "#1155cc", "#0b5394", "#351c75", "#741b47"],
                        ["#5b0f00", "#660000", "#783f04", "#7f6000", "#274e13", "#0c343d", "#1c4587", "#073763", "#20124d", "#4c1130"]
                ],
            hide: function(tinycolor) {
            	updateLinkWide(input[0],"linkColor");
            }
        });
};

/**
 * 获取table的的tr行数
 * 
 * @returns
 */
var getTrCount = function (){
	var table = $('#env_table');
	var tr = table.find('tr');
	var count = tr.length-1;
	return count;
};
/**
 * 初始化属性约束维护的属性显隐性
 * 
 * @param num
 */
var initDispl = function (num) {
    switch (num) {
        case '1':
        case '5':
            // 全部不可编辑
            updatDisplayClass([true, true, true, true, true, true]);
            return;
        case '2':
            updatDisplayClass([false, false, true, true, true, true]);
            return;
        case '3':
        case '4':
            updatDisplayClass([true, true, false, true, true, true]);
            return;

        case '6':
            updatDisplayClass([true, true, true, false, true, true]);
            return;
        case '7':
            updatDisplayClass([true, true, true, true, false, true]);
            return;
        case '8':
            updatDisplayClass([true, true, true, false, true, true]);
            return;
    }
};
/**
 * 决定显隐性
 * 
 * @param str
 */
var updatDisplayClass = function (str) {
    $('#min_val').attr("disabled", str[0]);
    $('#max_val').attr("disabled", str[1]);
    $('#reg_exp').attr("disabled", str[2]);
    $('#ext_cls').attr("disabled", str[3]);
    $('#rel_pro').attr("disabled", str[4]);
    $('#quote_pro').attr("disabled", str[5]);
};
/**
 * 初始化下拉
 * 
 * @param num
 */
var initSele = function (str) {
    var num = transformationNumFromStr(str);
    $('#rel_dir option').each(function (i, option) {
        var val = $(option).val();
        if (val == num)
            $(option).attr('selected', true);
    });
}
/**
 * 从字符串转为数字类型
 * 
 * @param str
 * @returns {number}
 */
var transformationNumFromStr = function (str) {
    switch (str) {
        case '源端--&gt;目的端':
            return 0;
        case '目的端--&gt;源端':
            return 1;
        case '源端&lt;--&gt;目的端':
            return 2;
    }
};

/**
 * 将数字类型转为字符串类型
 * 
 * @param num
 * @returns {*}
 */
var transformationStrFromNum = function (num) {
    switch (num) {
        case '0':
            return '源端--&gt;目的端';
        case '1':
            return '目的端--&gt;源端';
        case '2':
            return '源端&lt;--&gt;目的端';
    }
};

/**
 * 新增或修改环境约束
 * 
 * @param data
 */
var newOrUpdateEnv = function (data) {
	var opType ;
	var sourceProName = '';
	var descProName = '';
	
    if ($(data).length == 0) {
        // 新增
    	opType = "add";
        $('#sourceClsName').val('');
        $('#sourceProName').attr("disabled", true).val('');
// $('#sourceProName').val('');
        $('#descClsName').val('');
        $('#descProName').attr("disabled", true).val('');
// $('#').val('');
        $('#linkColor').val("rgb(255, 128, 0)");
    } else {
    	opType = "update";
        var tr = $(data).parent().parent();
        var classAndPro = tr.find('td input[type="hidden"]').val();
        $('#sourceClsName').val(tr.children('td').eq('0').html());
        sourceProName =  tr.children('td').eq('1').html();
        $('#descClsName').val(tr.children('td').eq('3').html());
        descProName = tr.children('td').eq('4').html();
        $('#linkColor').val(tr.children('td').eq('5').children('input').val());
        initSele(tr.find('td').eq('2').html());
    }
    $('#linkColor').spectrum({
        chooseText: "确定",
        cancelText: "取消",
        preferredFormat: "rgb",
        showInput: true,
        showPalette: true,
        palette:[
            ["#000000", "#434343", "#666666", "#999999", "#b7b7b7", "#cccccc", "#d9d9d9", "#efefef", "#f3f3f3", "#ffffff"],
                    ["#980000", "#ff0000", "#ff9900", "#ffff00", "#00ff00", "#00ffff", "#4a86e8", "#0000ff", "#9900ff", "#ff00ff"],
                    ["#e6b8af", "#f4cccc", "#fce5cd", "#fff2cc", "#d9ead3", "#d9ead3", "#c9daf8", "#cfe2f3", "#d9d2e9", "#ead1dc"],
                    ["#dd7e6b", "#ea9999", "#f9cb9c", "#ffe599", "#b6d7a8", "#a2c4c9", "#a4c2f4", "#9fc5e8", "#b4a7d6", "#d5a6bd"],
                    ["#cc4125", "#e06666", "#f6b26b", "#ffd966", "#93c47d", "#76a5af", "#6d9eeb", "#6fa8dc", "#8e7cc3", "#c27ba0"],
                    ["#a61c00", "#cc0000", "#e69138", "#f1c232", "#6aa84f", "#45818e", "#3c78d8", "#3d85c6", "#674ea7", "#a64d79"],
                    ["#85200c", "#990000", "#b45f06", "#bf9000", "#38761d", "#134f5c", "#1155cc", "#0b5394", "#351c75", "#741b47"],
                    ["#5b0f00", "#660000", "#783f04", "#7f6000", "#274e13", "#0c343d", "#1c4587", "#073763", "#20124d", "#4c1130"]
            ],
        hide: function(tinycolor) {
//        	updateLinkWide(input[0],"linkColor");
        }
    });
//    $('#sourceClsTr').removeClass();
//	 $('#sour_pro_Tr').removeClass();
//	 $('#descClsNameTr').removeClass();
//	 $('#desc_tr').removeClass();
    removeAllClass();

    document.getElementById('light').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    var divLi = document.getElementById('light');
    $(divLi).height(250);
    var divTop = getHeight();
    $(divLi).css("top",(divTop-125)+'px');
    var modeData = eval('('+$('#modeData').val()+")");
    
    initRel_pro(modeData, 'sourceClsName','sourceProName');
    initRel_pro(modeData, 'descClsName','descProName');
    
    var isConRel = $('#isConRel').val();
    $("#sourceClsName").change(function(){
    	$('#source_pro_td').empty();
    	$('#source_pro_td').append('<input class="xjinp" id="sourceProName" autocomplete="off" type="text" data-provide="typeahead" />');
//    	var sourClassName = $(this).val();
//    	if(checkBootModel(sourClassName,modeData)){
//    		sourClassName = sourClassName.substring(sourClassName.indexOf('(')+1,sourClassName.indexOf(')'));
//    		updateProNameInput(sourClassName,'sourceProName');
//    	}
    	if(isConRel == "0" || isConRel==0){
        	$('#sourceProName').attr("disabled", false);
        }else{
        	$('#sourceProName').attr("disabled", true);
        }
    	bindProValue('sourceProName','sour_pro_Tr','先输入正确的源端类型名称！','sourceClsName');
    	var clsName = $('#sourceClsName').val();
	  	 if(!checkBootModel(clsName,modeData)){
			  return ;
		  }else{
			  clsName = clsName.substring(clsName.indexOf('(')+1,clsName.indexOf(')'));
			  getAllProValue('sourceProName',clsName);
		  }
    	
    	
    });
    $("#sourceClsName").change();
    $('#sourceProName').val(sourceProName);
    
    $("#descClsName").change(function(){
    	$('#desc_pro_td').empty();
    	$('#desc_pro_td').append('<input class="xjinp" id="descProName" autocomplete="off" type="text" data-provide="typeahead" />');
    	var descClsName = $(this).val();
    	if(isConRel == "0" || isConRel==0){
        	$('#descProName').attr("disabled", false);
        }else{
        	$('#descProName').attr("disabled", true);
        }
    	bindProValue('descProName','desc_tr','先输入正确的目的端类型名称！','descClsName');
    	var clsName = $('#descClsName').val();
	  	 if(!checkBootModel(clsName,modeData)){
			  return ;
		  }else{
			  clsName = clsName.substring(clsName.indexOf('(')+1,clsName.indexOf(')'));
			  getAllProValue('descProName',clsName);
		  }
    });
    $("#descClsName").change();
    $('#descProName').val(descProName);
    
    // 点击确定按钮
    $('#env_sub').bind('click', function () {
//    	 $('#sourceClsTr').removeClass();
//    	 $('#sour_pro_Tr').removeClass();
//    	 $('#descClsNameTr').removeClass();
//    	 $('#desc_tr').removeClass();
    	removeAllClass();
    	var checkType = checkIsNull();
    	if(!checkType){
    		return false;
    	}
        var flag = submitConstaintMaintain(data,opType);
        if(flag){
            document.getElementById('light').style.display = 'none';
            document.getElementById('fade').style.display = 'none';
            $(this).unbind('click');
            $("#sourceClsName").unbind('change');
        }else{
        	/*alert('维护适用环境失败！');*/
        }
    });

    $('#env_ext').bind('click', function () {
        document.getElementById('light').style.display = 'none';
        document.getElementById('fade').style.display = 'none';
        $('#con_sub').unbind('click');
        $("#sourceClsName").unbind('change');
    });
};

/**
 * 属性判断
 * @param inputId 属性id
 * @param trName tr ID
 * @param msg 错误消息提示
 *  @param parObj 归属对象
 * 
 */
var bindProValue = function (inputId,trName,msg,parObj){
//	updateProNameInput('sourceProName');
	var isConRel = $('#isConRel').val();
	if(isConRel == '1' || isConRel == 1){
		// 为关系模型，属性不可选择，全部为资源ID（RESID）
		  $('#'+inputId).val("资源ID(resID)");
	}else{
		 $("#"+inputId).click(function(){
			 var clsName = $('#'+parObj).val();
		  	 var modeData = eval('('+$('#modeData').val()+")");
		  	 if(!checkBootModel(clsName,modeData)){
				  $('#'+trName).addClass("errortd");
				  $('#'+trName).find('td:last-child').find('div')
							.html(msg);
			  }
		  });
		    $('#'+inputId).blur(function (){
				$('#'+trName).removeClass();
			});
	}
}
/**
 * 遮罩层高都
 * @returns
 */
function getHeight(){
	var liulnaqiHei =  (parent.browerHei)/2; //浏览器可视化高度
    var scroHei = $(window.parent).scrollTop();  //滚动条高度
//    var parh = 52;
   return scroHei+liulnaqiHei+43;
}
/**
 * 移除所有的错误提示样式
 * @returns
 */
var removeAllClass = function (){
	 $('#sourceClsTr').removeClass();
	 $('#sour_pro_Tr').removeClass();
	 $('#descClsNameTr').removeClass();
	 $('#desc_tr').removeClass();
};
/**
 * 检查前端页面必填选项是否为空
 * 
 * @returns
 */
var checkIsNull = function(){
	var flag = true;
	var sourceClsName = $('#sourceClsName').val().replace(/^\s+|\s+$/g,"");
	var sourceProName = $('#sourceProName').val().replace(/^\s+|\s+$/g,"");
//	var direction = $('#rel_dir').val().replace(/^\s+|\s+$/g,"");
	var descClsName = $('#descClsName').val().replace(/^\s+|\s+$/g,"");
	var descProName = $('#descProName').val().replace(/^\s+|\s+$/g,"");
	 var modeData = eval('('+$('#modeData').val()+")");
	 
	 var sourceProMata =[];
	 var descProMata =[];
	 var temp1 = $('#sourceProMata').val();
	 var temp2 = $('#descProMata').val();
	 try{
		 sourceProMata = eval('('+temp1.split(',')+")");
		 descProMata = eval('('+temp2.split(',')+")");
	 }catch (e){
		 console.log("sourceProMata或descProMata处理异常！");
	 }
	if(sourceClsName == ""){
		$('#sourceClsTr').addClass("errortd");
		$('#sourceClsTr').find('td:last-child').find('div')
				.html("不能为空！");
		flag = false;
	}else if(!checkBootModel(sourceClsName,modeData)){
		$('#sourceClsTr').addClass("errortd");
		$('#sourceClsTr').find('td:last-child').find('div')
				.html("源端类型不存在！");
		flag = false;
	}
	if(descClsName == ""){
		$('#descClsNameTr').addClass("errortd");
		$('#descClsNameTr').find('td:last-child').find('div')
				.html("不能为空！");
		flag = false;
	}else if(!checkBootModel(descClsName,modeData)){
		$('#descClsNameTr').addClass("errortd");
		$('#descClsNameTr').find('td:last-child').find('div')
				.html("目的端类型不存在！");
		flag = false;
	}
	if(!flag){
		return flag;
	}
	var isConRel = $('#isConRel').val();
	
	if(isConRel == '1' || isConRel == 1){
		// 为关系模型，属性不可选择，全部为资源ID（RESID）
		return flag;	
	}
	
	if(sourceProName == ""){
		$('#sour_pro_Tr').addClass("errortd");
		$('#sour_pro_Tr').find('td:last-child').find('div')
				.html("不能为空！");
		flag = false;
	}else if(!checkBootModel(sourceProName,sourceProMata)){
		$('#sour_pro_Tr').addClass("errortd");
		$('#sour_pro_Tr').find('td:last-child').find('div')
				.html("源端属性不存在！");
		flag = false;
	}
	if(descProName == ""){
		$('#desc_tr').addClass("errortd");
		$('#desc_tr').find('td:last-child').find('div')
				.html("不能为空！");
		flag = false;
	}else if(!checkBootModel(descProName,descProMata)){
		$('#desc_tr').addClass("errortd");
		$('#desc_tr').find('td:last-child').find('div')
				.html("目的端属性不存在！");
		flag = false;
	}
	return flag;
}



/**
 * 判断数组中是否包含输入的值
 * @param modelName  输入的值
 * @param modeData  数组对象
 * @returns {boolean}
 */
var checkBootModel = function (modelName,modeData){
    var  flag = false;
    //modeData 格式如下：
    //["状态(phase)-$-zhuangtai","父对象(parentResID)-$-fuduixiang","创建人员(createdAccount)-$-chuangjianrenyuan","资源类型(resCategory)-$-ziyuanleixing","CI配置管理员(resCiAdmin)-$-CIpeizhiguanliyuan","关键级别(priority)-$-guanjianjibie","最后修改人员(lastUpdatedAccount)-$-zuihouxiugairenyuan"];
   ;
    $.each(modeData, function(i, tempData) {
        var temp1 = tempData.split('-\$-');
        if(modelName == temp1[0]){
            flag = true;
            return false;
        }
    });
    return flag;
};

/**
 * 删除一行数据
 * 
 * @param data
 * @returns
 */
var deletEnv_row = function (data){
	var flag = confirm("确定删除吗？");
	if(!flag){
		return ;
	}
	var relationEnvID = $(data).parent().next().find("input[type='hidden']").val();
	var classTitle = $('#classTitle').val();
	var flag = false;
	var basePath = $("#basePath").val();
  　　var url =basePath+"servlet/DeleteServlet";  
  var userAccount = $('#userAccount').val();
  	$.ajax({
		type:"POST",
		url:url,
		dataType:"json",
		data:{"classTitle":encodeURI(classTitle),
			"opClass":encodeURI("ResDefExtRelationEnv"),
			"relationEnvID":encodeURI(relationEnvID),
			"userAccount":encodeURI(userAccount)},
		async: false,
		success:function(returnValue)
			{
			flag =  returnValue.deletType;
			},
		error:function(){
			alert("错误");	
		}
	});
	if(flag){
		refushData();
	}else{
		alert("删除失败！");
	}
}
/**
 * 更新属性输入框
 * 
 * @param id
 * @returns
 */
//var updateProNameInput = function(id){
//	
//};

/**
 * 获取所有的属性值
 * @param inputId 属性id
 * @param inputId 所属对象resiD
 * 
 */
var getAllProValue = function (inputId,clsTitleAndName){
	var basePath = $("#basePath").val();
  　　var url =basePath+"servlet/ProGetServlet";  
  var userAccount = $('#userAccount').val();
  	$.ajax({
		type:"POST",
		url:url,
		data:{"userAccount":encodeURI(userAccount),
			"classTitle":encodeURI(clsTitleAndName),
			"proType":encodeURI('9')},
		dataType:"json",
		// contentType:"application/json;charset=utf-8",
		async: false,
		success:function(returnValue)
			{
			
			if(inputId=='sourceProName'){
				//源端属性
				$('#sourceProMata').val(JSON.stringify(returnValue.pro));
			}else if(inputId == 'descProName'){
				$('#descProMata').val(JSON.stringify(returnValue.pro));
			}
			 $("#" + inputId).typeahead({
			        source: returnValue.pro,  // 绑定数据源
			        highlighter: function (item) {
			            return item.split("-\$-")[0];
			        },
			        updater: function (item) {
			            return item.split("-\$-")[0];
			        },
			        afterSelect: function (item) {
			        }
			    });
			},
		error:function(){
			alert("错误");	
		}
	});
}
/**
 * 单个修改适用环境属性值
 */
var updateLinkWide = function (data,updatePro){

	var cur = $(data).attr("id");
	var classTitle  = $('#classTitle').val();
//	var updatePro = "linkColor";
	var opClass = 'ResDefExtRelationEnv';
	var updateVal = $(data).val().replace(/\s/g, "");
	var relationEnvID = $(data).parent().next().next().find('input[type="hidden"]').val();
	var isUpdate = false;
	var basePath = $("#basePath").val();
  　　var url =basePath+"servlet/UpdateClassServlet";  
  var userAccount = $('#userAccount').val();
  	$.ajax({
		type:"POST",
		url:url,
		dataType:"json",
		data:{"opClass":encodeURI(opClass),
			"classTitle":encodeURI(classTitle),
			"updatePro":encodeURI(updatePro),
			"updateVal":encodeURI(updateVal),
			"relationEnvID":encodeURI(relationEnvID),
			"optionType":encodeURI("updatePro"),
			"userAccount":encodeURI(userAccount)},
		async: false,
		success:function(returnValue)
			{
			isUpdate =  returnValue.newOrupdate;
			},
		error:function(){
			alert("错误");	
		}
	});
  	return isUpdate;
};

var trHeight = function (){
	 IFrameResizeByHeight("520px");
}
/**
 * 设置窗体高度
 * 
 * 
 */
var setTrWidth = function (){
//	var tempHeight =0;
//	$('#leftTable').find('tr').each(function(i,tr){
//		if($(tr).is(':visible')){
//			tempHeight += $(tr).height();
//		}
//	});
	var browser = $('#browser').height()+65;
	if(browser < 600){
		browser = 600;
	}
	IFrameResizeByHeight(browser+"px");
};

/**
 * 返回最大值
 */
var getMax_val = function (val1,val2){
	return (val1>val2)?val1:val2;
}

var searchData = function (){
	 var searchCode = $('#searchCode').val().replace(/(^\s*)|(\s*$)/g, "");
	/* if(searchCode==''){
		 //没有输入内容,显示所有的列表
		 
		 return ;
	 }*/
	 
	 $('#env_table').find('tr').each(function(i,tr){
		 if(i == 0){
			 return ;
		 }
//		 var input = $(tr).find("td").eq(7).find("input[type='hidden']").eq(1).val();
		 var childVal = $(tr).find("td:last-child input:last-child").val();
		 if(childVal.toLowerCase().indexOf(searchCode.toLowerCase()) > -1){
			 $(tr).show();
		 }else{
			 $(tr).hide();
		 }
	 });
}