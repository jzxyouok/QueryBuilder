<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>自定义高级查询</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!-- 新 Bootstrap 核心 CSS 文件 -->
<!-- <link rel="stylesheet"
	href="//cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap.min.css">
 -->
<!-- 可选的Bootstrap主题文件（一般不用引入） -->
<%-- <link rel="stylesheet" href="<%=path%>/css/bootstrap-theme.min.css"> --%>
<link rel="stylesheet" href="<%=path%>/css/main.css">

<!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
<script src="<%=path%>/js/jquery.min.js"></script>

<link href="<%=path %>/css/base.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/cmdb4.0.css" rel="stylesheet" type="text/css" />
<link href="<%=path %>/css/queryTemplate.css" rel="stylesheet"
	type="text/css" />
<script type="text/javascript" src="<%=path %>/js/template.js"></script>
</head>

<body>
	<div class="dis_pro">
		<input type="hidden" id="path" value="<%=path %>" />
	</div>
	<div class="tbtree">
		<div class="tbopera">
			<div class="tbbtns">
				<a href="#">刷新</a><a href="#">导出</a><a href="#">生成查询视图</a>
			</div>
			<div class="tbsearch">
				<input type="text" name="" id="" value="" /><a href="#">搜索</a>
			</div>
		</div>
		<div class="tbtable">
			<table id="templateData" width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<th scope="col" width="300">模型名称</th>
					<th scope="col">英文名称</th>
					<th scope="col">操作</th>
				</tr>
				<tr>
					<td>
						<div class="tbdiv tdcurrent">
							<span class="spanl fuxuan"><input type="checkbox" /></span> <span
								class="spanl tbimg"><img
								src="css/images/icon/mag20_2.png" /></span> <span class="spanl tbname">资源对象</span>
							<span class="spanl"><a class="tbedit" href="#"></a></span> <span
								class="spanr"><s class="tbdown"></s></span>
						</div>
					</td>
					<td>ResObject</td>
					<td class="tda"><a href="#">添加</a><a href="#">删除</a><a
						href="#">修改</a><a href="#">查看</a></td>
				</tr>
				<tr>
					<td>
						<div class="tbdiv tdcurrent" style="padding-left:1em;">
							<span class="spanl fuxuan"><input type="checkbox" /></span> <span
								class="spanl tbimg"><img
								src="css/images/icon/mag20_2.png" /></span> <span class="spanl tbname"><input
								class="tbinpt" type="text" name="" id="" /></span> <span class="spanl"><a
								class="tbedit" href="#"></a></span> <span class="spanr"><s
								class="tbdown"></s></span>
						</div>
					</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<div class="tbdiv tdcurrent" style="padding-left:2em;">
							<span class="spanl fuxuan"><input type="checkbox" /></span> <span
								class="spanl tbimg"><img
								src="css/images/icon/mag20_2.png" /></span> <span class="spanl tbname"><input
								class="tbinpt" type="text" name="" id="" /></span> <span class="spanl"><a
								class="tbedit" href="#"></a></span> <span class="spanr"><s
								class="tbdown"></s></span>
						</div>
					</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<div class="tbdiv" style="padding-left:3em;">
							<span class="spanl fuxuan"><input type="checkbox" /></span> <span
								class="spanl tbimg"><img
								src="css/images/icon/mag20_2.png" /></span> <span class="spanl tbname"><input
								class="tbinpt" type="text" name="" id="" /></span> <span class="spanl"><a
								class="tbedit" href="#"></a></span> <span class="spanr"
								style="display:none"><s class="tbdown"></s></span>
						</div>
					</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<div class="tbdiv">
							<span class="spanl fuxuan"><input type="checkbox" /></span> <span
								class="spanl tbimg"><img
								src="css/images/icon/mag20_2.png" /></span> <span class="spanl tbname">dsfsdf</span>
							<span class="spanl"><a class="tbedit" href="#"></a></span> <span
								class="spanr"><s class="tbdown"></s></span>
						</div>
					</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>

		</div>
	</div>
<input id="gegtfad" />
</body>
</html>
